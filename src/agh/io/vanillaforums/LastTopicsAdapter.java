package agh.io.vanillaforums;

import java.util.List;
import java.util.Map;

import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class LastTopicsAdapter extends BaseAdapter {
 
    private List<IFlatTopic> listData;
    private Map<Integer, String> parents;
 
    private LayoutInflater layoutInflater;
 
    public LastTopicsAdapter(Context context, List<IFlatTopic> listData, Map<Integer, String> parents) {
        this.listData = listData;
        this.parents = parents;
        layoutInflater = LayoutInflater.from(context);
    }
 
    @Override
    public int getCount() {
        return listData.size();
    }
 
    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_last_topics, null);
            holder = new ViewHolder();
            holder.titleView = (TextView) convertView.findViewById(R.id.list_last_topic_title);
            holder.parentView = (TextView) convertView.findViewById(R.id.list_last_topic_parent);
            holder.dateView = (TextView) convertView.findViewById(R.id.list_last_topic_date);
            convertView.setTag(holder);
            
            IFlatTopic topic = listData.get(position);
            
            if (topic.hasChanged()) {
            	convertView.setBackgroundColor(Color.rgb(242, 187, 102));
            }
            
            holder.titleView.setText(topic.getTitle());
            System.out.println(topic.getParentForumId() + " " + parents.get(topic.getParentForumId()));
            holder.parentView.setText(parents.get(topic.getParentForumId()));
            holder.dateView.setText(topic.getUpdateDate().toString());
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
 
        return convertView;
    }
 
    static class ViewHolder {
        TextView titleView;
        TextView parentView;
        TextView dateView;
    }
 
}