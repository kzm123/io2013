package agh.io.vanillaforums;

import java.util.List;

import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CategoryForumsAdapter extends BaseAdapter {
 
    private List<IFlatForum> listData;
 
    private LayoutInflater layoutInflater;
 
    public CategoryForumsAdapter(Context context, List<IFlatForum> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }
 
    @Override
    public int getCount() {
        return listData.size();
    }
 
    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_forums, null);
            holder = new ViewHolder();
            holder.titleView = (TextView) convertView.findViewById(R.id.list_forum_title);
            holder.descriptionView = (TextView) convertView.findViewById(R.id.list_forum_description);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
 
        holder.titleView.setText(listData.get(position).getName());
        holder.descriptionView.setText(listData.get(position).getDescription());
 
        return convertView;
    }
 
    static class ViewHolder {
        TextView titleView;
        TextView descriptionView;
    }
 
}