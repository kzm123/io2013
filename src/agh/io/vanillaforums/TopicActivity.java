package agh.io.vanillaforums;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import agh.io.vanillaforums.transition.ContentRemoteManager;
import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.IFlatComment;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.iteminfo.CommentInfo;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

public class TopicActivity extends Activity {

	private IContentRemoteManager remoteManager;
	private int id;
	EditText text;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_topic);
		
		text = (EditText) findViewById(R.id.comment_input);
		
		remoteManager = ContentRemoteManager.getDefaultInstance();

		Bundle extras = getIntent().getExtras();
		setTitle(extras.getString("name"));
		id = extras.getInt("id");
		
		new RefreshCommentsTask().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.topic, menu);
		MenuItem item = menu.getItem(1);
		item.setChecked(MyApplication.myTopics.contains(Integer.valueOf(id)));
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_authors:
			showAlertDialog(R.string.authors_title, R.string.authors_desc);
			break;
		case R.id.action_settings:
			Intent settingsIntent = new Intent(this, SettingsActivity.class);
			startActivity(settingsIntent);
			break;
		case R.id.action_last_topics:
			Intent lastTopicsIntent = new Intent(this, LastTopicsActivity.class);
			startActivity(lastTopicsIntent);
			break;
		case R.id.action_subscribe:
			if (item.isChecked()) {
				try {
					MyApplication.myTopics.remove(Integer.valueOf(id));
					item.setChecked(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				try {
					MyApplication.myTopics.add(Integer.valueOf(id));
					item.setChecked(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void showAlertDialog(int titleId, int messageId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(messageId).setTitle(titleId);
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	private void showErrorDialog(int messageId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(messageId).setTitle(R.string.error);
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	public class RefreshCommentsTask extends AsyncTask<Object, Object, Object> {
		
		private ProgressDialog dialog = new ProgressDialog(TopicActivity.this);
		
		@Override
		protected void onPreExecute() {
			dialog.setMessage("Please wait...");
			dialog.show();
		}
		
		@Override
		protected void onPostExecute(Object result) {
			if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
		}

		@Override
		protected Object doInBackground(Object... params) {
			refresh();
			return null;
		}
		
		private void refresh() {
			try {
				final List<IFlatComment> comments = remoteManager.getTopicComments(id);
				final Map<Integer, IFlatUser> users = new HashMap<Integer, IFlatUser>();
				
				IFlatTopic topic = remoteManager.getTopic(id);
				MyApplication.localTopics.put(id, topic);
				
				for (IFlatComment comment : comments) {
					users.put(comment.getCreatorId(), remoteManager.getUser(comment.getCreatorId()));
				}
				
		
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						final ListView listView = (ListView) findViewById(R.id.list_comments);
						TopicCommentsAdapter adapter = new TopicCommentsAdapter(TopicActivity.this, comments, users);
						listView.setAdapter(adapter);
					}
				});
				

				
			} catch (NotAuthorizedException e) {
				Intent intent = new Intent(TopicActivity.this, LoginActivity.class);
				startActivity(intent);
			} catch (Exception e) {
				if (dialog.isShowing()) {
		            dialog.dismiss();
		        }
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						showErrorDialog(R.string.error_connection);
					}
				});
				
			}
		}
	}
	
	public void sendComment(View v) {
		new CreateCommentTask().execute();
	}
	
	public class CreateCommentTask extends AsyncTask<Object, Object, Object> {
		
		@Override
		public void onPostExecute(Object result) {
			new RefreshCommentsTask().execute();
		}

		@Override
		protected Object doInBackground(Object... params) {
			try {
				CommentInfo comment = new CommentInfo();
				comment.setCommentBody(text.getText().toString());
				comment.setTopicId(id);
				
				runOnUiThread(new Runnable() {	
					@Override
					public void run() {
						text.setText("");
					}
				});
				
				remoteManager.createComment(id, comment);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}
	}
	
}
