package agh.io.vanillaforums;

import java.util.List;
import java.util.Map;

import agh.io.vanillaforums.transition.interfaces.items.IFlatComment;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TopicCommentsAdapter extends BaseAdapter {
 
    private List<IFlatComment> listData;
 
    private LayoutInflater layoutInflater;
    Map<Integer, IFlatUser> users;
 
    public TopicCommentsAdapter(Context context, List<IFlatComment> listData, Map<Integer, IFlatUser> users) {
        this.listData = listData;
        this.users = users;
        layoutInflater = LayoutInflater.from(context);
    }
 
    @Override
    public int getCount() {
        return listData.size();
    }
 
    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_comments, null);
            holder = new ViewHolder();
            holder.userView = (TextView) convertView.findViewById(R.id.comment_user);
            holder.dateView = (TextView) convertView.findViewById(R.id.comment_date);
            holder.textView = (TextView) convertView.findViewById(R.id.comment_body);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

		holder.userView.setText(users.get(listData.get(position).getCreatorId()).getName());
		holder.dateView.setText(listData.get(position).getUpdateDate().toString());
		holder.textView.setText(listData.get(position).getBody());
 
        return convertView;
    }
 
    static class ViewHolder {
        TextView userView;
        TextView dateView;
        TextView textView;
    }
 
}