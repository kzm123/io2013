package agh.io.vanillaforums.transition.interfaces.managing;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpRequestBase;


public interface IRequestManager {
	public HttpResponse executeRequest(HttpRequestBase request) throws ClientProtocolException, IOException;
}
