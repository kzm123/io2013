package agh.io.vanillaforums.transition.interfaces.managing;

import java.util.List;

import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.AlreadyExistsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.ForumInfo;



/*
 * Operacje na forach
 */
public interface IForumRemoteManager {

	public List<IFlatForum> getAllForums() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException;
	public List<IFlatForum> getCategoryForums(int categoryId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,  InvalidRecordException;
	public IFlatForum getForum(int forumId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,  InvalidRecordException;
	public IFlatForum createForum(ForumInfo forumInfo) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, AlreadyExistsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException;
	public IFlatForum updateForum(int forumId, ForumInfo forumNewInfo) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException;
	public boolean deleteForum(int forumId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;
	
}