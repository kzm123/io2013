package agh.io.vanillaforums.transition.interfaces.managing.exceptions;

public class UnprocessableEntityException extends Exception {

	private static final long serialVersionUID = -5585021683994048140L;

	public UnprocessableEntityException(){
		super("Unsuccessful create, update, or delete; syntactic or validation error");
	}
}
