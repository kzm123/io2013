package agh.io.vanillaforums.transition.interfaces.managing.exceptions;

public class InternalServerErrorException extends Exception {

	private static final long serialVersionUID = 1538513524434571853L;

	public InternalServerErrorException(){
		super("Unexpected error; you’ve likely found a bug in LiquidPlanner");
	}
}
