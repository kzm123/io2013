package agh.io.vanillaforums.transition.interfaces.managing.exceptions;

public class ServiceUnavaliableException extends Exception {

	private static final long serialVersionUID = 4954747559087024611L;

	
	public ServiceUnavaliableException(){
		super("Service Unavailable");
	}
}
