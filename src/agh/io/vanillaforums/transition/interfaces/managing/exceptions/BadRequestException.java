package agh.io.vanillaforums.transition.interfaces.managing.exceptions;

public class BadRequestException extends Exception {

	private static final long serialVersionUID = -791965155417694756L;

	public BadRequestException(String url){
		super("Unrecognized or invalid request URL : " + url);
	}
}
