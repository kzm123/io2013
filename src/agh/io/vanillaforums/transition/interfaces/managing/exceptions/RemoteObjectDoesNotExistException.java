package agh.io.vanillaforums.transition.interfaces.managing.exceptions;

import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;

public class RemoteObjectDoesNotExistException extends Exception {

	private ContentType _contentType;
	private int _objectId;
	
	private static final long serialVersionUID = -6561652512512161762L;

	
	public RemoteObjectDoesNotExistException(ContentType contentType, int objectId){
		super("Remote object of type : " + contentType.toString() +" and id : " + objectId + " does not exist");
		
		_objectId = objectId;
		_contentType = contentType;
	}
	
	public int getObjectId(){
		return _objectId;
	}
	
	public ContentType getContentType(){
		return _contentType;
	}
}
