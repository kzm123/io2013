package agh.io.vanillaforums.transition.interfaces.managing.exceptions;

public class NotImplementedException extends Exception {

	private static final long serialVersionUID = 1308241675393065595L;

	
	public NotImplementedException(){
		super("You asked for an API version that is not implemented (either because it does not exist, or it has been deprecated)");
	}
}
