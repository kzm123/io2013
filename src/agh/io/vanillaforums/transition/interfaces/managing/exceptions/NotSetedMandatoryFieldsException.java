package agh.io.vanillaforums.transition.interfaces.managing.exceptions;

import java.util.HashSet;
import java.util.Set;

import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.FieldType;

public class NotSetedMandatoryFieldsException extends Exception {

	private static final long serialVersionUID = 3545495368258724797L;

	private Set<FieldType> _unsetedFields = new HashSet<FieldType>();
	public ContentType _parentItem;
	
	public NotSetedMandatoryFieldsException(ContentType item){
		super("There are mandatory fields, which must be seted");
		_parentItem = item;
	}
	
	public NotSetedMandatoryFieldsException addUsetedField(FieldType field){
		_unsetedFields.add(field);
		return this;
	}
	
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer(super.toString());
		
		sb.append(" Fields : ");
		for(FieldType ft : _unsetedFields )
			 sb.append("\""+ft.getValue()+ "\" ");
		
		return sb.toString();
	}
	
	public ContentType getFieldsOwner(){
		return _parentItem;
	}
	
	public Set<FieldType> getUnsetedFields(){
		return _unsetedFields;
	}
}
