package agh.io.vanillaforums.transition.interfaces.managing.exceptions;

public class UnrecognizedRemoteErrorException extends Exception {

	private static final long serialVersionUID = 498536740623292202L;

	private String _errorName;
	private String _errorDescription;
	
	public UnrecognizedRemoteErrorException(String errorName, String errorDescr){
		super("Remote error : "+ errorName + " (" + errorDescr + ")");
		_errorDescription = errorDescr;
		_errorName = errorName;
	}
	
	public String getErrorName(){
		return _errorName;
	}
	
	public String getErrorDescription(){
		return _errorDescription;
	}
}
