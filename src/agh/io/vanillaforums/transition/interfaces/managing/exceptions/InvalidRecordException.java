package agh.io.vanillaforums.transition.interfaces.managing.exceptions;

import java.util.HashSet;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.utils.Constants;

public class InvalidRecordException extends Exception {

	private static final long serialVersionUID = -4850716777575552803L;

	private String _description;
	private HashSet<String> _cause = new HashSet<String>();
	
	public InvalidRecordException(JSONObject specification){
		try {
			
			StringBuffer sb = new StringBuffer(specification.getString(Constants.JSON_IVALID_RECORD_DESCRIPTION));
	
			JSONObject detailsJson = specification.getJSONObject(Constants.JSON_IVALID_RECORD_DETAILS);
			@SuppressWarnings("unchecked")
			Iterator<String> fieldIt = detailsJson.keys();
			
			for(;fieldIt.hasNext();){
				String fieldName = fieldIt.next();
				sb.append("\nfield \""+fieldName+"\" : ");
				JSONArray issueArr = detailsJson.getJSONArray(fieldName);
				
				for(int i = 0; i < issueArr.length(); ++i){
					JSONObject attr = issueArr.getJSONObject(i);
					@SuppressWarnings("unchecked")
					Iterator<String> attrName = attr.keys();
				
					while(attrName.hasNext()){
						String nameInner = attrName.next();
						String value = attr.getString(nameInner);
						sb.append(value + " ;");
						_cause.add(value);
					}
				}
				
				sb.append('\n');
			}
	
			_description = sb.toString();
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
		
	@Override
	public String toString(){
		return super.toString() +" : " + _description;
	}
	
	public void throwProperException() throws AlreadyExistsException{
		if(_cause.contains(Constants.JSON_IVALID_RECORD_ERR_DESCR_NAME_ALREADY_IN_USE)){
			throw new AlreadyExistsException();
		}
	
	}
}
	
