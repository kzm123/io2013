package agh.io.vanillaforums.transition.interfaces.items.parsing;

import org.json.JSONObject;

import agh.io.vanillaforums.transition.impl.FlatForumSubscription;
import agh.io.vanillaforums.transition.impl.FlatTopicSubscription;

public interface IFlatSubscriptionsJSONParser {
	public FlatForumSubscription parseForumSubscription(JSONObject jsonSubscription);
	public FlatTopicSubscription parseTopicSubscription(JSONObject jsonSubscription);
}
