package agh.io.vanillaforums.transition.interfaces.items.parsing;

import org.json.JSONObject;

import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;

public interface IFlatTopicJSONParser {
	public IFlatTopic parseTopic(JSONObject jsnObj);
}
