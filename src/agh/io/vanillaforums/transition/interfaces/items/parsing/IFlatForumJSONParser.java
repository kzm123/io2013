package agh.io.vanillaforums.transition.interfaces.items.parsing;

import org.json.JSONObject;

import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;

public interface IFlatForumJSONParser {
	public IFlatForum parseForum(JSONObject jsnObj);
}
