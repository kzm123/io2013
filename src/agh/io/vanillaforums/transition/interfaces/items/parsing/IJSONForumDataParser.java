package agh.io.vanillaforums.transition.interfaces.items.parsing;

import agh.io.vanillaforums.transition.IContentRemoteManager;

public interface IJSONForumDataParser extends IFlatCategoryJSONParser,
		IFlatCommentJSONParser, IFlatForumJSONParser, IFlatTopicJSONParser,
		IFlatUserJSONParser, IFlatSubscriptionsJSONParser {

	public void setContentRemoteManager(IContentRemoteManager manager);
}
