package agh.io.vanillaforums.transition.interfaces.items.parsing;

import org.json.JSONObject;

import agh.io.vanillaforums.transition.interfaces.items.IFlatCategory;

public interface IFlatCategoryJSONParser {
	public IFlatCategory parseCategory(JSONObject jsnObj);
}
