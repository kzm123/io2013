package agh.io.vanillaforums.transition.interfaces.items.parsing;

import org.json.JSONObject;

import agh.io.vanillaforums.transition.interfaces.items.IFlatComment;

public interface IFlatCommentJSONParser {
	public IFlatComment parseComment(JSONObject jsnObj);
}
