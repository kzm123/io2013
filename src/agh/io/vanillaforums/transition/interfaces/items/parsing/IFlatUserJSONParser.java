package agh.io.vanillaforums.transition.interfaces.items.parsing;

import org.json.JSONObject;

import agh.io.vanillaforums.transition.impl.FlatUserRelatedInfo;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;

public interface IFlatUserJSONParser {
	public IFlatUser parseUser(JSONObject jsnObj);
	public FlatUserRelatedInfo parseUserRelatedInfo(JSONObject jsonInfo, int ownerUserId);
}
