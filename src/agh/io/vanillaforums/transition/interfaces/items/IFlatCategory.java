package agh.io.vanillaforums.transition.interfaces.items;

import java.util.List;

import agh.io.vanillaforums.transition.interfaces.managing.exceptions.AlreadyExistsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.CategoryInfo;
import agh.io.vanillaforums.transition.iteminfo.ForumInfo;

public interface IFlatCategory extends ICommon
{
    /*
     * Operacje działające zdalnie
     */
    public List<IFlatForum> getForums() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            NotFoundException, InvalidRecordException;

    public IFlatForum getForum( int forumId ) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            NotFoundException, InvalidRecordException;

    public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;

    public boolean update( CategoryInfo newInfo ) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException,
            NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
            NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException;

    public IFlatForum createForum( ForumInfo forumInfo ) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException,
            AlreadyExistsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
            NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException;

    public String getName();

    public String getDescription();

    public int getPosition();

    public CategoryInfo getInfo();

    public boolean hasChanged();

    public void setHasChanged( boolean hasChanged );
}
