package agh.io.vanillaforums.transition.interfaces.items;

import java.io.UnsupportedEncodingException;
import java.util.List;

import agh.io.vanillaforums.transition.interfaces.managing.exceptions.AlreadyExistsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.ForumInfo;
import agh.io.vanillaforums.transition.iteminfo.TopicInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.ForumAccessType;
import agh.io.vanillaforums.transition.iteminfo.utils.ForumType;

public interface IFlatForum extends ICommon
{
    /*
     * Operacje zdalne (wykorzystują połączenie internetowe)
     */
    public List<IFlatTopic> getTopics() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;

    public boolean update( ForumInfo forumNewInfo ) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException,
            NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
            NotImplementedException, InternalServerErrorException, InvalidRecordException;

    public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;

    public IFlatCategory parentCategory() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;

    public IFlatTopic createTopic( TopicInfo topicInfo ) throws NotSetedMandatoryFieldsException, NotAuthorizedException,
            BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException,
            InternalServerErrorException, NotFoundException, AlreadyExistsException, InvalidRecordException;

    public boolean lockForum() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException;

    public boolean unlockForum() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException;

    public IFlatForumSubscription subscribeFor( IFlatUser subscriber ) throws UnsupportedEncodingException, NotAuthorizedException,
            BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException,
            InternalServerErrorException, NotFoundException, InvalidRecordException;

    /*
     * Operacje lokalne
     */
    public int getParentCategoryId();

    public String getName();

    public String getDescription();

    public int getPosition();

    public ForumAccessType getAccessType();

    public ForumType getType();

    public boolean isLocked();

    public ForumInfo getInfo();

    public boolean hasChanged();

    public void setHasChanged( boolean hasChanged );
}
