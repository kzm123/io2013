package agh.io.vanillaforums.transition.interfaces.items;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import agh.io.vanillaforums.transition.impl.FlatUserRelatedInfo;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.UserInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.UserRole;

public interface IFlatUser extends ICommon{
	
	/*
	 *  Metody lokalne
	 */
	
	public String getName();
	public boolean isActive();
	public Date lastLoginAt();
	public String getEmail();
	public String getPhone();
	public String getDetails();
	public UserRole getRole();
	public boolean isModerator();
	public boolean isSuspended();
	public String getSignature();
	public UserInfo getInfo();
	
	/*
	 *	Metody zdalne 
	 */
	
	public IFlatForumSubscription subscribeForum(IFlatForum targetForum) throws UnsupportedEncodingException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException;
	public IFlatTopicSubscription subscribeTopic(IFlatTopic targetTopic) throws UnsupportedEncodingException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException;
	public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;
	public boolean update(UserInfo info) throws NotSetedMandatoryFieldsException, UnsupportedEncodingException, RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;
	public FlatUserRelatedInfo getRelatedInfo() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;
	public boolean changePassword(String oldPassword, String newPassword) throws RemoteObjectDoesNotExistException, UnsupportedEncodingException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException;
	public List<IFlatTopic> getUserTopics() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;
	public List<IFlatComment> getUserComments() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException;
}
