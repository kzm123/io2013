package agh.io.vanillaforums.transition.interfaces.items;

import java.io.UnsupportedEncodingException;
import java.util.List;

import agh.io.vanillaforums.transition.interfaces.managing.exceptions.AlreadyExistsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.CommentInfo;
import agh.io.vanillaforums.transition.iteminfo.TopicInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.ForumType;

public interface IFlatTopic extends ICommon
{
    /*
     * Operacje zdalne
     */
    public List<IFlatComment> getComments() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException;

    public IFlatForum getParentForum() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException;

    public IFlatUser getCreator() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;

    public IFlatUser getLastUpdator() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;

    public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;

    public boolean update( TopicInfo topicInfo ) throws RemoteObjectDoesNotExistException, NotSetedMandatoryFieldsException,
            NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
            NotImplementedException, InternalServerErrorException, InvalidRecordException;

    public IFlatComment addComment( CommentInfo commentInfo ) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException,
            AlreadyExistsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
            NotImplementedException, InternalServerErrorException, InvalidRecordException;

    public boolean lockTopic() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException;

    public boolean unlockTopic() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException;

    public IFlatTopicSubscription subscribeFor( IFlatUser subscriber ) throws UnsupportedEncodingException, NotAuthorizedException,
            BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException,
            InternalServerErrorException, NotFoundException, InvalidRecordException;

    /*
     * Operacje lokalne
     */
    public String getTitle();

    public String getBody();

    public int getSubmitterId();

    public int getLastUpdatorId();

    public int getParentForumId();

    public int getCommentsCount();

    public boolean isLocked();

    public int getPosition();

    public ForumType getTopicType();

    public TopicInfo getInfo();

    public boolean hasChanged();

    public void setHasChanged( boolean hasChanged );
}
