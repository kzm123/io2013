package agh.io.vanillaforums.transition.interfaces.items;

import java.util.Date;

import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;




public interface ICommon {
	/*
	 * Zdalne
	 */
	
	/*  Zwraca true, jeśli ten obiekt udało się odświeżyć, 
	 *	false jeśli on już nie istnieje
	 */
	public boolean reloadIfExists() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException;
	
	/*
	 * Lokalne
	 */
	
	public Date getCreationDate();
	public Date getUpdateDate();
	public int getId();
	
}