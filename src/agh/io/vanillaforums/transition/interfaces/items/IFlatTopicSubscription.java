package agh.io.vanillaforums.transition.interfaces.items;

import java.util.Date;

import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;



public interface IFlatTopicSubscription {
	/*
	 *  Metody zdalne
	 */
	public IFlatTopic getTopic() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;
	public IFlatUser getUser() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;
	public boolean delete() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, RemoteObjectDoesNotExistException;
	
	/*
	 *  Metody lokalne
	 */
	public Date getCreationDate();
	public int getId();
	public int getUserId();
	public int getTopicId();
}
