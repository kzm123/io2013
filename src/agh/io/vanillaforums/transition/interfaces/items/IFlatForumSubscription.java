package agh.io.vanillaforums.transition.interfaces.items;

import java.util.Date;

import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;



public interface IFlatForumSubscription {
	/*
	 *  Metody zdalne
	*/
	public IFlatForum getForum() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException;
	public IFlatUser getUser() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;
	public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException;
	
	/*
	 *  Metody lokalne
	 */
	
	public int getId();
	public Date getCreationDate();
	public int getForumId();
	public int getUserId();
}
