package agh.io.vanillaforums.transition.utils;

public interface IAPIUrlBuilder {
	/*
	 * Category urls
	 */
	public String listCategoriesUrl();
	public String showCategoryUrl(int categoryId);
	public String createCategoryUrl();
	public String updateCategoryUrl(int categoryId);
	public String deleteCategoryUrl(int categoryId);
	
	/*
	 * Forum urls
	 */
	public String listForumsUrl();
	public String listForumsUrl(int categoryId);
	public String showForumUrl(int forumId);
	public String createForumUrl();
	public String updateForumUrl(int forumId);
	public String deleteForumUrl(int forumId);
	
	/*
	 *  Topic urls
	 */
	public String listTopicsUrl();
	public String listTopicsOfForumUrl(int forumId);
	public String listTopicsOfUsetUrl(int userId);
	public String showTopicUrl(int topicId);
	public String createTopicUrl();
	public String updateTopicUrl(int topicId);
	public String deleteTopicUrl(int topicId);
	
	/*
	 *  Comment urls
	 */
	
	public String listCommentsTopicUrl(int topicId);
	public String listCommentsUserUrl(int userId);
	public String showCommentTopicUrl(int topicId, int commentId);
	public String showCommentUserUrl(int userId, int commentId);
	public String createCommentUrl(int topicId);
	public String updateCommentUrl(int topicId, int commentId);
	public String deleteCommentUrl(int topicId, int commentId);
	
	/*
	 *  User urls
	 */
	public String listUsersUrl();
	public String showUserUrl(int userId);
	public String showUserRelatedInfoUrl(int userId);
	public String createUserUrl();
	public String updateUserUrl(int userId);
	public String deleteUserUrl(int userId);
	public String setUserPasswordUrl(int userId);
	public String changerUsersPasswordUrl(int userId);
	
	/*
	 * Subscription urls
	 */
	
	public String listForumSubscriptionsUrl(int forumId);
	public String listForumSubscriptionsUrl();
	public String showForumSubscriptionUrl(int subscriptionId);
	public String createForumSubscriptionUrl();
	public String deleteForumSubscriptionUrl(int subscriptionId);
	
	public String listTopicSubscriptionsUrl(int topicId);
	public String listTopicSubscriptionsUrl();
	public String showTopicSubscriptionUrl(int subscriptionId);
	public String createTopicSubscriptionUrl();
	public String deleteTopicSubscriptionUrl(int subscriptionId);
	
	
}
