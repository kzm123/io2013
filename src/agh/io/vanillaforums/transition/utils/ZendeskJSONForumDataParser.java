package agh.io.vanillaforums.transition.utils;

import java.util.Date;

import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.impl.FlatCategory;
import agh.io.vanillaforums.transition.impl.FlatComment;
import agh.io.vanillaforums.transition.impl.FlatForum;
import agh.io.vanillaforums.transition.impl.FlatForumSubscription;
import agh.io.vanillaforums.transition.impl.FlatTopic;
import agh.io.vanillaforums.transition.impl.FlatTopicSubscription;
import agh.io.vanillaforums.transition.impl.FlatUser;
import agh.io.vanillaforums.transition.impl.FlatUserRelatedInfo;
import agh.io.vanillaforums.transition.interfaces.items.IFlatCategory;
import agh.io.vanillaforums.transition.interfaces.items.IFlatComment;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;
import agh.io.vanillaforums.transition.iteminfo.utils.ForumAccessType;
import agh.io.vanillaforums.transition.iteminfo.utils.ForumType;
import agh.io.vanillaforums.transition.iteminfo.utils.UserRole;

public class ZendeskJSONForumDataParser implements IJSONForumDataParser {

	
	private IContentRemoteManager _remoteManager;

	@Override
	public IFlatCategory parseCategory(JSONObject jsnObj) {
		int catedoryID;
		String apiUrl;
		String name;
		String description;
		Date createdAt;
		Date updatedAt;
		int position;
		
		try {
			catedoryID = jsnObj.getInt(Constants.ID_ATTR);
			position = jsnObj.getInt(Constants.POSITION_ATTR);
			name = jsnObj.getString(Constants.NAME_ATTR);
			description = jsnObj.getString(Constants.DESCRIPTION_ATTR);
			apiUrl = jsnObj.getString(Constants.URL_ATTR);
			createdAt = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.CREATED_AT_ATTR));
			updatedAt = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.UPDATED_AT_ATTR));
			
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return new FlatCategory(name, description, catedoryID, position, apiUrl, createdAt, updatedAt, _remoteManager);
	}

	@Override
	public IFlatForum parseForum(JSONObject jsnObj) {
		String url;
		int id;
		String name;
		String description;
		int parentCategory;
		boolean isLocked;
		int position;
		ForumType type;
		ForumAccessType accessType;
		Date createdAt;
		Date updatedAt;
		
		try {
			url = jsnObj.getString(Constants.URL_ATTR);
			id = jsnObj.getInt(Constants.ID_ATTR);
			name = jsnObj.getString(Constants.NAME_ATTR);
			description = jsnObj.getString(Constants.DESCRIPTION_ATTR);
			parentCategory = jsnObj.getInt(Constants.FORUM_PARENT_CATEGORY_ATTR);
			isLocked = jsnObj.getBoolean(Constants.LOCKED_ATTR);
			position = jsnObj.getInt(Constants.POSITION_ATTR);
			createdAt = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.CREATED_AT_ATTR));
			updatedAt = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.UPDATED_AT_ATTR));
			type = ForumType.getTypeByName(jsnObj.getString(Constants.FORUM_TYPE_ATTR));
			accessType = ForumAccessType.getTypeByName(jsnObj.getString(Constants.FORUM_ACCESS_TYPE_ATTR));
			
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return new FlatForum(id, name, description, parentCategory, isLocked, position, accessType, type, createdAt, updatedAt, url, _remoteManager);
	}
		
	@Override
	public IFlatComment parseComment(JSONObject jsnObj) {
		
		
		int id;
		String body;
		int userId;
		int topicId;
		Date creationDate;
		Date updateDate;
		String apiUrl;
		
		try{
			
			id = jsnObj.getInt(Constants.ID_ATTR);
			body = jsnObj.getString(Constants.TOPIC_BODY_ATTR);
			userId = jsnObj.getInt(Constants.COMMENT_CREATOR_ID_ATTR);
			topicId = jsnObj.getInt(Constants.COMMENT_TOPIC_ID_ATTR);
			apiUrl = jsnObj.getString(Constants.URL_ATTR);
			creationDate = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.CREATED_AT_ATTR));
			updateDate = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.UPDATED_AT_ATTR));
			
		}catch(JSONException e){
			e.printStackTrace();
			return null;
		}
		
		
		return new FlatComment(id, body, userId, topicId, creationDate, updateDate, apiUrl, _remoteManager);
	}

	@Override
	public IFlatTopic parseTopic(JSONObject jsnObj) {
		
		String apiUrl;
		Date updateDate;
		Date creationDate;
		boolean locked;
		int position;
		int commentsCount;
		int parentForum;
		int updaterId;
		int submitterId;
		ForumType topicType;
		String body;
		String title;
		int id;
		
		
		try{
			id = jsnObj.getInt(Constants.ID_ATTR);
			title = jsnObj.getString(Constants.TOPIC_TITLE_ATTR);
			body = jsnObj.getString(Constants.TOPIC_BODY_ATTR);
			topicType = ForumType.getTypeByName(jsnObj.getString(Constants.TOPIC_TOPIC_TYPE));
			submitterId = jsnObj.getInt(Constants.TOPIC_SUBMITTER_ID);
			updaterId = jsnObj.getInt(Constants.TOPIC_UPDATER_ATTR);
			parentForum = jsnObj.getInt(Constants.TOPIC_PARENT_FORUM_ATTR);
			commentsCount = jsnObj.getInt(Constants.TOPIC_COMMENTS_COUNT_ATTR);
			position = (jsnObj.isNull(Constants.POSITION_ATTR))? -1 : jsnObj.getInt(Constants.POSITION_ATTR);
			locked = jsnObj.getBoolean(Constants.LOCKED_ATTR);
			creationDate = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.CREATED_AT_ATTR));
			updateDate = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.UPDATED_AT_ATTR));
			apiUrl = jsnObj.getString(Constants.URL_ATTR);
			
		}catch(JSONException e){
			e.printStackTrace();
			return null;
		}
		
		
		return new FlatTopic(id, title, body, topicType, submitterId, updaterId, parentForum, commentsCount, position, locked, creationDate, updateDate, apiUrl, _remoteManager);
	}

	@Override
	public IFlatUser parseUser(JSONObject jsnObj) {
		int id;
		Date creationDate;
		Date updateDate;
		String apiUrl;
		String name;
		boolean isActive;
		Date lastLoginAt;
		String email;
		String phone;
		String details;
		UserRole userRole;
		boolean isSuspended;
		boolean isModerator;
		String signature;
		
		try {
			
			id = jsnObj.getInt(Constants.ID_ATTR);
			creationDate = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.CREATED_AT_ATTR));
			System.out.println("OK1");
			updateDate = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.UPDATED_AT_ATTR));
			System.out.println("OK2");
			apiUrl = jsnObj.getString(Constants.URL_ATTR);
			name = jsnObj.getString(Constants.NAME_ATTR);
			isActive = jsnObj.getBoolean(Constants.USER_ACTIVE_ATTR);
			if(jsnObj.isNull(Constants.USER_LAST_LOGIN_AT_ATTR))
				lastLoginAt = new Date(0);
			else
				lastLoginAt = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsnObj.getString(Constants.USER_LAST_LOGIN_AT_ATTR));
			System.out.println("OK3");
			email = jsnObj.getString(Constants.USER_EMAIL_ATTR);
			phone = jsnObj.getString(Constants.USER_PHONE_ATTR);
			details = jsnObj.getString(Constants.USER_DETAILS_ATTR);
			userRole = UserRole.forName(jsnObj.getString(Constants.USER_ROLE_ATTR));
			isSuspended = jsnObj.getBoolean(Constants.USER_SUSPENDED_ATTR);
			isModerator = jsnObj.getBoolean(Constants.USER_MODERATOR_ATTR);
			signature = jsnObj.getString(Constants.USER_SIGNATURE_ATTR);
			
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return new FlatUser(id, creationDate, updateDate, apiUrl, name, isActive, lastLoginAt, email, phone, details, userRole, isSuspended, isModerator, signature, _remoteManager);
	}
 
	
	@Override
	public void setContentRemoteManager(IContentRemoteManager manager) {
		_remoteManager = manager;
	}

	
	@Override
	public FlatUserRelatedInfo parseUserRelatedInfo(JSONObject jsonInfo, int ownerUserId) {
		int topics_count;
		int topics_comments_count;
		int votes_count;
		int subscriptions_count;
		
		try {
			
			topics_count = jsonInfo.getInt(Constants.USER_RELATED_TOPICS_COUNT_ATTR);
			topics_comments_count = jsonInfo.getInt(Constants.USER_RELATED_TOPIC_COMENNTS_COUNT_ATTR);
			votes_count = jsonInfo.getInt(Constants.USER_RELATED_VOTES_COUNT_ATTR);
			subscriptions_count = jsonInfo.getInt(Constants.USER_RELATED_SUBSCRIPTIONS_COUNT_ATTR);
			
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return new FlatUserRelatedInfo(subscriptions_count, topics_count, topics_comments_count, votes_count, _remoteManager.getAPIUrlBuilder().showUserRelatedInfoUrl(ownerUserId), _remoteManager);
	}

	
	@Override
	public FlatForumSubscription parseForumSubscription(JSONObject jsonSubscription) {
		int id;
		int forum_id;
		int user_id;
		Date creationDate;
		
		try {
			id = jsonSubscription.getInt(Constants.ID_ATTR);
			forum_id = jsonSubscription.getInt(Constants.SUBSCRIPTION_FORUM_FORUM_ID_ATTR);
			user_id = jsonSubscription.getInt(Constants.SUBSCRIPTION_USER_ID_ATTR);
			creationDate = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsonSubscription.getString(Constants.SUBSCRIPTION_CREATION_DATE_ATTR));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return new FlatForumSubscription(id, creationDate, forum_id, user_id, _remoteManager);
	}

	
	@Override
	public FlatTopicSubscription parseTopicSubscription(JSONObject jsonSubscription) {
		int id;
		int topic_id;
		int user_id;
		Date creationDate;
		
		try {
			id = jsonSubscription.getInt(Constants.ID_ATTR);
			topic_id = jsonSubscription.getInt(Constants.SUBSCRIPTION_TOPIC_TOPIC_ID_ATTR);
			user_id = jsonSubscription.getInt(Constants.SUBSCRIPTION_USER_ID_ATTR);
			creationDate = Constants.SERVICE_UTIL_API_DATE_PARSER.parseDate(jsonSubscription.getString(Constants.SUBSCRIPTION_CREATION_DATE_ATTR));
		} catch (JSONException e) {
			e.printStackTrace();
			return null;
		}
		
		return new FlatTopicSubscription(id, creationDate, topic_id, user_id, _remoteManager);
	}


}
