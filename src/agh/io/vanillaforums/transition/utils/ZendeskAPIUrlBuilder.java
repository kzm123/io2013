package agh.io.vanillaforums.transition.utils;


public class ZendeskAPIUrlBuilder implements IAPIUrlBuilder {

	private static String __api_prefix = Constants.SERVICE_ADDRESS + '/' + Constants.SERVICE_API_PREFIX + '/';
	
	private StringBuffer getApiPrefix(){
		StringBuffer sb = new StringBuffer(__api_prefix);
		return sb;
	}
	
	@Override
	public String listCategoriesUrl() {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_CATEGORIES);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String showCategoryUrl(int categoryId) {
		StringBuffer sb = getApiPrefix(); 
		sb.append(Constants.SERVICE_API_CATEGORIES);
		sb.append('/');
		sb.append(categoryId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String createCategoryUrl() {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_CATEGORIES);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	@Override
	public String updateCategoryUrl(int categoryId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_CATEGORIES);
		sb.append('/');
		sb.append(categoryId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String deleteCategoryUrl(int categoryId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_CATEGORIES);
		sb.append('/');
		sb.append(categoryId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	
	
	
	@Override
	public String listForumsUrl() {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_FORUMS);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	@Override
	public String listForumsUrl(int categoryId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_CATEGORIES);
		sb.append('/');
		sb.append(categoryId);
		sb.append('/');
		sb.append(Constants.SERVICE_API_FORUMS);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String showForumUrl(int forumId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_FORUMS);
		sb.append('/');
		sb.append(forumId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String createForumUrl() {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_FORUMS);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String updateForumUrl(int forumId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_FORUMS);
		sb.append('/');
		sb.append(forumId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String deleteForumUrl(int forumId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_FORUMS);
		sb.append('/');
		sb.append(forumId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	
	
	
	@Override
	public String listTopicsUrl() {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String listTopicsOfForumUrl(int forumId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_FORUMS);
		sb.append('/');
		sb.append(forumId);
		sb.append('/');
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String listTopicsOfUsetUrl(int userId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS);
		sb.append('/');
		sb.append(userId);
		sb.append('/');
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String showTopicUrl(int topicId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append('/');
		sb.append(topicId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String createTopicUrl() {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String updateTopicUrl(int topicId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append('/');
		sb.append(topicId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String deleteTopicUrl(int topicId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append('/');
		sb.append(topicId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	
	
	
	@Override
	public String listCommentsTopicUrl(int topicId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append('/');
		sb.append(topicId);
		sb.append('/');
		sb.append(Constants.SERVICE_API_COMMENTS);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		
		return sb.toString();
	}

	@Override
	public String listCommentsUserUrl(int userId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS);
		sb.append('/');
		sb.append(userId);
		sb.append('/');
		sb.append(Constants.SERVICE_API_TOPIC_COMMENTS);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String showCommentTopicUrl(int topicId, int commentId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append('/');
		sb.append(topicId);
		sb.append('/');
		sb.append(Constants.SERVICE_API_COMMENTS);
		sb.append('/');
		sb.append(commentId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String showCommentUserUrl(int userId, int commentId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS);
		sb.append('/');
		sb.append(userId);
		sb.append('/');
		sb.append(Constants.SERVICE_API_TOPIC_COMMENTS);
		sb.append('/');
		sb.append(commentId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String createCommentUrl(int topicId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append('/');
		sb.append(topicId);
		sb.append('/');
		sb.append(Constants.SERVICE_API_COMMENTS);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String updateCommentUrl(int topicId, int commentId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append('/');
		sb.append(topicId);
		sb.append('/');
		sb.append(Constants.SERVICE_API_COMMENTS);
		sb.append('/');
		sb.append(commentId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	@Override
	public String deleteCommentUrl(int topicId, int commentId) {
		StringBuffer sb =  getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS);
		sb.append('/');
		sb.append(topicId);
		sb.append('/');
		sb.append(Constants.SERVICE_API_COMMENTS);
		sb.append('/');
		sb.append(commentId);
		sb.append(Constants.SERVICE_API_FILE_EXT);
		
		return sb.toString();
	}

	
	@Override
	public String listForumSubscriptionsUrl(int forumId) {
		StringBuffer sb = getApiPrefix();
		sb.append(forumId).append('/').append(Constants.SERVICE_API_SUBSCRIPTIONS).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	
	@Override
	public String listForumSubscriptionsUrl() {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_FORUM_SUBSCRIPTIONS).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	
	@Override
	public String showForumSubscriptionUrl(int subscriptionId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_FORUM_SUBSCRIPTIONS).append('/').append(subscriptionId).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	
	@Override
	public String createForumSubscriptionUrl() {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_FORUM_SUBSCRIPTIONS).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	
	@Override
	public String deleteForumSubscriptionUrl(int subscriptionId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_FORUM_SUBSCRIPTIONS).append('/').append(subscriptionId).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	
	@Override
	public String listTopicSubscriptionsUrl(int topicId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPICS).append('/').append(topicId).append('/').append(Constants.SERVICE_API_SUBSCRIPTIONS).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	
	@Override
	public String listTopicSubscriptionsUrl() {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPIC_SUBSCRIPTIONS).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	
	@Override
	public String showTopicSubscriptionUrl(int subscriptionId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPIC_SUBSCRIPTIONS).append('/').append(subscriptionId).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	
	@Override
	public String createTopicSubscriptionUrl() {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPIC_SUBSCRIPTIONS).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	
	@Override
	public String deleteTopicSubscriptionUrl(int subscriptionId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_TOPIC_SUBSCRIPTIONS).append('/').append(subscriptionId).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	@Override
	public String listUsersUrl() {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	@Override
	public String showUserUrl(int userId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS).append('/').append(userId).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	@Override
	public String showUserRelatedInfoUrl(int userId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS).append('/').append(userId).append('/').append(Constants.SERVICE_API_RELATED).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	@Override
	public String createUserUrl() {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	@Override
	public String updateUserUrl(int userId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS).append('/').append(userId).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	@Override
	public String deleteUserUrl(int userId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS).append('/').append(userId).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	@Override
	public String setUserPasswordUrl(int userId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS).append('/').append(userId).append('/').append(Constants.SERVICE_API_PASSWORD).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

	@Override
	public String changerUsersPasswordUrl(int userId) {
		StringBuffer sb = getApiPrefix();
		sb.append(Constants.SERVICE_API_USERS).append('/').append(userId).append('/').append(Constants.SERVICE_API_PASSWORD).append(Constants.SERVICE_API_FILE_EXT);
		return sb.toString();
	}

}
