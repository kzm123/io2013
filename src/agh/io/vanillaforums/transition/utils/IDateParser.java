package agh.io.vanillaforums.transition.utils;

import java.util.Date;

public interface IDateParser {
	public Date parseDate(String dateRepr);
	public String dateToString(Date date);
}
