package agh.io.vanillaforums.transition.utils;

import android.annotation.SuppressLint;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@SuppressLint("SimpleDateFormat")
public class ISO8601DateParser implements IDateParser {
	    
		// 2004-06-14T19:GMT20:30Z
	    // 2004-06-20T06:GMT22:01Z

	    // Different standards may need different levels of granularity in the date and
	    // time, so this profile defines six levels. Standards that reference this
	    // profile should specify one or more of these granularities. If a given
	    // standard allows more than one granularity, it should specify the meaning of
	    // the dates and times with reduced precision, for example, the result of
	    // comparing two dates with different precisions.

	    // The formats are as follows. Exactly the components shown here must be
	    // present, with exactly this punctuation. Note that the "T" appears literally
	    // in the string, to indicate the beginning of the time element, as specified in
	    // ISO 8601.

	    //    Year:
	    //       YYYY (eg 1997)
	    //    Year and month:
	    //       YYYY-MM (eg 1997-07)
	    //    Complete date:
	    //       YYYY-MM-DD (eg 1997-07-16)
	    //    Complete date plus hours and minutes:
	    //       YYYY-MM-DDThh:mmTZD (eg 1997-07-16T19:20+01:00)
	    //    Complete date plus hours, minutes and seconds:
	    //       YYYY-MM-DDThh:mm:ssTZD (eg 1997-07-16T19:20:30+01:00)
	    //    Complete date plus hours, minutes, seconds and a decimal fraction of a
	    // second
	    //       YYYY-MM-DDThh:mm:ss.sTZD (eg 1997-07-16T19:20:30.45+01:00)

	    // where:

	    //      YYYY = four-digit year
	    //      MM   = two-digit month (01=January, etc.)
	    //      DD   = two-digit day of month (01 through 31)
	    //      hh   = two digits of hour (00 through 23) (am/pm NOT allowed)
	    //      mm   = two digits of minute (00 through 59)
	    //      ss   = two digits of second (00 through 59)
	    //      s    = one or more digits representing a decimal fraction of a second
	    //      TZD  = time zone designator (Z or +hh:mm or -hh:mm)   
	
	@Override
	public Date parseDate(String dateRepr) {
		//NOTE: SimpleDateFormat uses GMT[-+]hh:mm for the TZ which breaks
        //things a bit.  Before we go on we have to repair this.
        SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssz" );
        
        //this is zero time so we need to add that TZ indicator for 
        if ( dateRepr.endsWith( "Z" ) ) {
            dateRepr = dateRepr.substring( 0, dateRepr.length() - 1) + "GMT-00:00";
        } else {
            int inset = 6;
        
            String s0 = dateRepr.substring( 0, dateRepr.length() - inset );
            String s1 = dateRepr.substring( dateRepr.length() - inset, dateRepr.length() );

            dateRepr = s0 + "GMT" + s1;
        }
        
        try {
			return df.parse( dateRepr );
		} catch (ParseException e) {
			e.printStackTrace();
		}
        
        return null;
	}

	@Override
	public String dateToString(Date date) {
		 SimpleDateFormat df = new SimpleDateFormat( "yyyy-MM-dd'T'HH:mm:ssz" );
	        
	        TimeZone tz = TimeZone.getTimeZone( "UTC" );
	        
	        df.setTimeZone( tz );

	        String output = df.format( date );

	        int inset0 = 9;
	        int inset1 = 6;
	        
	        String s0 = output.substring( 0, output.length() - inset0 );
	        String s1 = output.substring( output.length() - inset1, output.length() );

	        String result = s0 + s1;

	        result = result.replaceAll( "UTC", "+00:00" );
	        
	        return result;
	}

}
