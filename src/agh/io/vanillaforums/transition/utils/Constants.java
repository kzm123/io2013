package agh.io.vanillaforums.transition.utils;

public final class Constants {

	/*
	 * Attributes possible values
	 */
	
	public static final String FORUM_TYPE_ARTICLES = "articles";
	public static final String FORUM_TYPE_IDEAS = "ideas";
	public static final String FORUM_TYPE_QUESTIONS = "questions";


	public static final String FORUM_ACCESS_LI_USERS = "logged-in users";
	public static final String FORUM_ACCESS_EVERYBODY = "everybody";
	public static final String FORUM_ACCESS_AGENTS_ONLY = "agents only";


	/*================================================================
	 * Attributes names
	 */
	
	/*
	 *  Common attributes
	 */
	
	public static final String ID_ATTR = "id";
	public static final String URL_ATTR = "url"; 
	public static final String NAME_ATTR = "name";
	public static final String DESCRIPTION_ATTR = "description";
	public static final String POSITION_ATTR = "position";
	public static final String CREATED_AT_ATTR = "created_at";
	public static final String UPDATED_AT_ATTR = "updated_at";
	public static final String LOCKED_ATTR = "locked";
	
	/*
	 * Category attributes are the same as commons
	 */
	
	/*
	 * Forum attributes
	 */

	public static final String FORUM_PARENT_CATEGORY_ATTR = "category_id";
	public static final String FORUM_TYPE_ATTR = "forum_type";
	public static final String FORUM_ACCESS_TYPE_ATTR = "access";
	
	
	/*
	 * Topic attributes
	 */

	public static final String TOPIC_TITLE_ATTR = "title";
	public static final String TOPIC_BODY_ATTR = "body";
	public static final String TOPIC_PARENT_FORUM_ATTR = "forum_id";
	public static final String TOPIC_TOPIC_TYPE = "topic_type";
	public static final String TOPIC_COMMENTS_COUNT_ATTR = "comments_count";
	public static final String TOPIC_UPDATER_ATTR = "updater_id";
	public static final String TOPIC_SUBMITTER_ID = "submitter_id";
	
	
	/*
	 * Comment attributes
	 */
	
	public static final String COMMENT_TOPIC_ID_ATTR = "topic_id";
	public static final String COMMENT_CREATOR_ID_ATTR = "user_id";
	
	/*
	 *  Subsription attributes
	 */
	
	public static final String SUBSCRIPTION_FORUM_FORUM_ID_ATTR = "forum_id";
	public static final String SUBSCRIPTION_USER_ID_ATTR = "user_id";
	public static final String SUBSCRIPTION_CREATION_DATE_ATTR = "created_at";
	public static final String SUBSCRIPTION_TOPIC_TOPIC_ID_ATTR = "topic_id";
	
	/*
	 *  User related attributes
	 */
	
	public static final String USER_RELATED_TOPICS_COUNT_ATTR = "topics";
	public static final String USER_RELATED_TOPIC_COMENNTS_COUNT_ATTR = "topic_comments";
	public static final String USER_RELATED_SUBSCRIPTIONS_COUNT_ATTR = "subscriptions";
	public static final String USER_RELATED_VOTES_COUNT_ATTR = "votes";
	
	/*
	 * 	User attributes
	 */
	
	public static final String USER_ALIAS_ATTR = "alias";
	public static final String USER_ACTIVE_ATTR = "active";
	public static final String USER_LAST_LOGIN_AT_ATTR = "last_login_at";
	public static final String USER_EMAIL_ATTR = "email";
	public static final String USER_PHONE_ATTR = "phone";
	public static final String USER_SIGNATURE_ATTR = "signature";
	public static final String USER_DETAILS_ATTR = "details";
	public static final String USER_NOTES_ATTR = "notes";
	public static final String USER_ROLE_ATTR = "role";
	public static final String USER_MODERATOR_ATTR = "moderator";
	public static final String USER_SUSPENDED_ATTR = "suspended";
	
	public static final String USER_ROLE_ENDUSER = "end-user";
	public static final String USER_ROLE_AGENT = "agent";
	public static final String USER_ROLE_ADMIN = "admin";
	
	public static final String USER_PASSWORD_ATTR = "password";
	public static final String USER_OLD_PASSWORD_ATTR = "previous_password";
	
	/*====================================================================
	 * 						API constants
	 *====================================================================
	 */
	public static final String SERVICE_ADDRESS = "https://io2013restapi.zendesk.com";
	
	
	public static final String SERVICE_API_PREFIX = "api/v2";
	public static final String SERVICE_API_FILE_EXT = ".json";
	
	public static final String SERVICE_API_CATEGORIES = "categories";
	public static final String SERVICE_API_CATEGORY = "category";
	public static final String SERVICE_API_FORUMS = "forums";
	public static final String SERVICE_API_TOPICS = "topics";
	public static final String SERVICE_API_USERS = "users";
	public static final String SERVICE_API_RELATED = "related";
	public static final String SERVICE_API_PASSWORD = "password";
	public static final String SERVICE_API_TOPIC_COMMENTS =  "topic_comments";
	public static final String SERVICE_API_COMMENTS =  "comments";
	public static final String SERVICE_API_SUBSCRIPTIONS = "subscriptions";
	public static final String SERVICE_API_TOPIC_SUBSCRIPTIONS = "topic_subscriptions";
	public static final String SERVICE_API_FORUM_SUBSCRIPTIONS = "forum_subscriptions";
	
	/*
	 *  ===================================================================
	 *  					FORUM UTILS
	 *  ===================================================================
	 */
	
	public static final IDateParser SERVICE_UTIL_API_DATE_PARSER = new ISO8601DateParser();
	
	/*
	 * ====================================================================
	 * 						FORUM RESPONSE CODES
	 * ====================================================================
	 */
	/*
		Success Codes
		200 OK
		successful read, update, or delete
		
		201 Created
		successful create
		Error or Exception Codes
		
		400 Bad Request
		unrecognized or invalid request URL
		
		401 Unauthorized
		credentials not provided or are incorrect
		
		422 Unprocessable Entity
		unsuccessful create, update, or delete; syntactic or validation error
		
		404 Not Found
		record does not exist, or you do not have permission to access it
		
		500 Internal Server Error
		unexpected error; you’ve likely found a bug in LiquidPlanner
		
		501 Not Implemented
		you asked for an API version that is not implemented (either because it does not exist, or it has been deprecated)
		
		503 Service Unavailable
	*/
	
	public static final int SERVICE_API_RESPONSE_OK = 200;
	public static final int SERVICE_API_RESPONSE_CREATED = 201;
	public static final int SERVICE_API_RESPONSE_BAD_REQUEST = 400;
	public static final int SERVICE_API_RESPONSE_UNAUTHORISED = 401;
	public static final int SERVICE_API_RESPONSE_UNPROCESSABLE_ENTITY = 402;
	public static final int SERVICE_API_RESPONSE_NOT_FOUND = 404;
	public static final int SERVICE_API_RESPONSE_INVALID_RECORD = 422;
	public static final int SERVICE_API_RESPONSE_INTERNAL_ERROR = 500;
	public static final int SERVICE_API_RESPONSE_NOT_IMPLEMENTED = 501;
	public static final int SERVICE_API_RESPONSE_SERVICE_UNAVALIABLE = 503;
	
	/*=====================================================================
	 * 						JSON constants
	 * ====================================================================
	 */
	
	
	//===============Invalid record constants =============================
	public static final String JSON_IVALID_RECORD_DESCRIPTION = "description";
	public static final String JSON_IVALID_RECORD_DETAILS = "details";
	public static final String JSON_IVALID_RECORD_ERR_DESCR_BLANK_NAME = "Name cannot be blank";
	public static final String JSON_IVALID_RECORD_ERR_DESCR_NAME_ALREADY_IN_USE = "Name has already been taken";
	/*
	 * =====================================================================
	 * 					DEVELOPER	CREDENTIALS
	 * =====================================================================
	 */
	
	public static final String API_USERNAME = "io2013restapi@gmail.com";
	public static final String API_PASSWORD = "aghio2013";
	
	/*
	 * ===========================================================================
	 * 							REQUEST HEADERS
	 * ===========================================================================
	 */
	public static final String SERVICE_API_HEADER_ACCEPT = "Accept";
	public static final String SERVICE_API_HEADER_CONTENT_TYPE = "Content-type";
	public static final String SERVICE_API_HEADER_CONTENT_TYPE_VALUE = "application/json";
	
	/*
	 *============================================================================
	 *						REQUEST ENTITY CONTENT NAMES
	 * ===========================================================================	
	 */
	
	public static final String SERVICE_CONTENT_CATEGORY = "category";
	public static final String SERVICE_CONTENT_FORUM = "forum";
	public static final String SERVICE_CONTENT_USER = "user";
	public static final String SERVICE_CONTENT_TOPIC = "topic";
	public static final String SERVICE_CONTENT_COMMENT = "topic_comment";
	public static final String SERVICE_CONTENT_TOPIC_SUBSCRIPTION = "topic_subscription";	
	public static final String SERVICE_CONTENT_FORUM_SUBSCRIPTION = "forum_subscription";	
	public static final String SERVICE_CONTENT_USER_RELATED = "user_related";				
}
