package agh.io.vanillaforums.transition.utils;

import java.util.List;

import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;

public interface IContentJSONDataInterpreter {
	public ICommon getItem(String source, IJSONForumDataParser parser);
	public List<ICommon> getListOfItems(String source, IJSONForumDataParser parser);
	public IContentJSONDataInterpreter setProperty(String name, String value);
}
