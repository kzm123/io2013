package agh.io.vanillaforums.transition.utils;

import agh.io.vanillaforums.transition.impl.FlatCategory;
import agh.io.vanillaforums.transition.impl.FlatComment;
import agh.io.vanillaforums.transition.impl.FlatForum;
import agh.io.vanillaforums.transition.impl.FlatForumSubscription;
import agh.io.vanillaforums.transition.impl.FlatTopic;
import agh.io.vanillaforums.transition.impl.FlatTopicSubscription;
import agh.io.vanillaforums.transition.impl.FlatUser;
import agh.io.vanillaforums.transition.impl.FlatUserRelatedInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;

public enum ZendeskContentJSONIntepreters {
	CATEGORY_INT(ContentType.CATEGORY),
	FORUM_INT(ContentType.FORUM),
	TOPIC_INT(ContentType.TOPIC),
	COMMENT_INT(ContentType.COMMENT),
	USER_INT(ContentType.USER),
	USER_RELATER_INT(ContentType.USER_RELATED),
	TOPIC_SUBSCRIPTION_INT(ContentType.TOPIC_SUBSCRIPTION),
	FORUM_SUBSCRIPTION_INT(ContentType.FORUM_SUBSCRIPTION),
	;
	
	private IContentJSONDataInterpreter _interpreter;
	
	private ZendeskContentJSONIntepreters(ContentType contentType){
		switch(contentType){
		case CATEGORY :{
			_interpreter = FlatCategory.getInterpreter();
			break;
		}
		case FORUM :{
			_interpreter = FlatForum.getInterpreter();
			break;
		}
		case TOPIC:{
			_interpreter = FlatTopic.getInterpreter();
			break;
		}
		case COMMENT:{
			_interpreter = FlatComment.getInterpreter();
			break;
		}
		case FORUM_SUBSCRIPTION:{
			_interpreter = FlatForumSubscription.getInterpreter();
			break;
		}
		case TOPIC_SUBSCRIPTION:{
			_interpreter = FlatTopicSubscription.getInterpreter();
			break;
		}
		case USER:{
			_interpreter = FlatUser.getInterpreter();
			break;
		}
		case USER_RELATED:{
			_interpreter = FlatUserRelatedInfo.getInterpreter();
			break;
		}
		}
	}
	
	public IContentJSONDataInterpreter getValue(){
		return _interpreter;
	}
}
