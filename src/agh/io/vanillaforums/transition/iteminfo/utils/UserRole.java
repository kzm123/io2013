package agh.io.vanillaforums.transition.iteminfo.utils;

import agh.io.vanillaforums.transition.utils.Constants;

public enum UserRole {
	ADMIN(Constants.USER_ROLE_ADMIN),
	AGENT(Constants.USER_ROLE_AGENT),
	END_USER(Constants.USER_ROLE_ENDUSER)
	;
	
	private String _type;

	private UserRole(String userType){
		_type = userType;
	}
	
	public String getValue(){
		return _type;
	}
	
	public static UserRole forName(String roleName){
		if(roleName.equals(Constants.USER_ROLE_ADMIN))
			return ADMIN;
		if(roleName.equals(Constants.USER_ROLE_AGENT))
			return AGENT;
	//	if(roleName.equals(Constants.USER_ROLE_ENDUSER))
		return END_USER;
		
	}
}
