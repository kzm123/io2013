package agh.io.vanillaforums.transition.iteminfo.utils;

import java.util.HashSet;


public class FieldsManager {

	private HashSet<FieldType> _fields = new HashSet<FieldType>();
	
	public FieldsManager setField(FieldType field){
		_fields.add(field);
		return this;
	}
	
	public boolean isSeted(FieldType field){
		return _fields.contains(field);
	}
}
