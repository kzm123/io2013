package agh.io.vanillaforums.transition.iteminfo.utils;

import agh.io.vanillaforums.transition.utils.Constants;

public enum ForumAccessType {
	ACC_LOGGED_IN_USERS(Constants.FORUM_ACCESS_LI_USERS),
	ACC_EVERYBODY(Constants.FORUM_ACCESS_EVERYBODY),
	ACC_AGENTS_ONLY(Constants.FORUM_ACCESS_AGENTS_ONLY)
	;
	
	private String _type;
	
	private ForumAccessType(String type){
		_type = type;
	}
	
	public String getValue(){
		return _type;
	}
	
	public static ForumAccessType getTypeByName(String name){
		if(name.equals(Constants.FORUM_ACCESS_LI_USERS))
			return ACC_LOGGED_IN_USERS;
		else if(name.equals(Constants.FORUM_ACCESS_EVERYBODY))
			return ACC_EVERYBODY;
		else
			return ACC_AGENTS_ONLY;
	}
	
}
