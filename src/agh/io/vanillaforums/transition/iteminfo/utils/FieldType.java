package agh.io.vanillaforums.transition.iteminfo.utils;

import agh.io.vanillaforums.transition.utils.Constants;

public enum FieldType {
	FIELD_ID(Constants.ID_ATTR),
	FIELD_NAME(Constants.NAME_ATTR),
	FIELD_DESCRIPTION(Constants.DESCRIPTION_ATTR), 
	FIELD_POSITION(Constants.POSITION_ATTR),
	FIELD_LOCKED(Constants.LOCKED_ATTR), 
	FIELD_FORUM_TYPE(Constants.FORUM_TYPE_ATTR), 
	FIELD_FORUM_ACCESS_TYPE(Constants.FORUM_ACCESS_TYPE_ATTR), 
	FIELD_FORUM_PARENT_CATEGORY(Constants.FORUM_PARENT_CATEGORY_ATTR),
	FIELD_BODY(Constants.TOPIC_BODY_ATTR), 
	FIELD_TOPIC_TITLE(Constants.TOPIC_TITLE_ATTR), 
	FIELD_PARENT_FORUM_ID(Constants.TOPIC_PARENT_FORUM_ATTR), 
	FIELD_TOPIC_ID(Constants.COMMENT_TOPIC_ID_ATTR), 
	FIELD_CREATOR_ID(Constants.COMMENT_CREATOR_ID_ATTR),
	FIELD_ALIAS(Constants.USER_ALIAS_ATTR),
	FIELD_EMAIL(Constants.USER_EMAIL_ATTR),
	FIELD_PHONE(Constants.USER_PHONE_ATTR),
	FIELD_SIGNATURE(Constants.USER_SIGNATURE_ATTR),
	FIELD_DETAILS(Constants.USER_DETAILS_ATTR),
	FIELD_ROLE(Constants.USER_ROLE_ATTR),
	FIELD_MODERATOR(Constants.USER_MODERATOR_ATTR),
	FIELD_SUSPENDED(Constants.USER_SUSPENDED_ATTR)
	;
	
	private String _attrName;

	private FieldType(String attrName){
		_attrName = attrName;
	}
	
	
	public String getValue(){
		return _attrName;
	}
	
}
