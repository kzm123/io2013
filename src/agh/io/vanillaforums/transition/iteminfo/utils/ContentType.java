package agh.io.vanillaforums.transition.iteminfo.utils;

import agh.io.vanillaforums.transition.utils.Constants;

public enum ContentType {
	CATEGORY(Constants.SERVICE_CONTENT_CATEGORY),
	FORUM(Constants.SERVICE_CONTENT_FORUM),
	USER(Constants.SERVICE_CONTENT_USER),
	TOPIC(Constants.SERVICE_CONTENT_TOPIC),
	COMMENT(Constants.SERVICE_CONTENT_COMMENT),
	TOPIC_SUBSCRIPTION(Constants.SERVICE_CONTENT_TOPIC_SUBSCRIPTION),
	FORUM_SUBSCRIPTION(Constants.SERVICE_CONTENT_FORUM_SUBSCRIPTION),
	USER_RELATED(Constants.SERVICE_CONTENT_USER_RELATED)
	;
	
	private String _typeName;

	private ContentType(String contentName){
		_typeName = contentName;
	}
	
	public String getValue(){
		return _typeName;
	}
}
