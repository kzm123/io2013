package agh.io.vanillaforums.transition.iteminfo.utils;

import agh.io.vanillaforums.transition.utils.Constants;

public enum ForumType {
	ARTICLES(Constants.FORUM_TYPE_ARTICLES),
	IDEAS(Constants.FORUM_TYPE_IDEAS),
	QUESTIONS(Constants.FORUM_TYPE_QUESTIONS)
	;
	
	private String _value;

	private ForumType(String type){
		_value = type;
	}
	
	public String getValue(){
		return _value;
	}
	
	public static ForumType getTypeByName(String name){
		if(name.equals(Constants.FORUM_TYPE_ARTICLES))
			return ARTICLES;
		else if (name.equals(Constants.FORUM_TYPE_IDEAS))
			return IDEAS;
		else
			return QUESTIONS;
	}
}
