package agh.io.vanillaforums.transition.iteminfo;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.FieldType;
import agh.io.vanillaforums.transition.iteminfo.utils.ForumAccessType;
import agh.io.vanillaforums.transition.iteminfo.utils.ForumType;



public class ForumInfo extends ContentItemInfo {

	static{
		__mandatoryFieldsSet = new HashSet<FieldType>();
		__updateFieldsSet = new HashSet<FieldType>();
		
		__mandatoryFieldsSet.add(FieldType.FIELD_NAME);
		__mandatoryFieldsSet.add(FieldType.FIELD_FORUM_PARENT_CATEGORY);
		
		__updateFieldsSet.addAll(__mandatoryFieldsSet);
		__updateFieldsSet.add(FieldType.FIELD_ID);
	}
	
	
	private String _forumName;
	private String _forumDescription;
	private int _parentCategoryId;
	private boolean _isLocked;
	private int _positionOfForum;
	private ForumType _forumType;		
	private ForumAccessType _forumAccsess;
	
	
	
	public ForumInfo() {
		super(ContentType.FORUM);
	}
	
	public ForumInfo setForumName(String forumName){
		_forumName = forumName;
		_fieldsManager.setField(FieldType.FIELD_NAME);
		
		return this;
	}
	
	public ForumInfo setForumDescription(String forumDescString){
		_forumDescription = forumDescString;
		_fieldsManager.setField(FieldType.FIELD_DESCRIPTION);
		
		return this;
	}
	
	public ForumInfo setIsLocked(boolean isLocked){
		_isLocked = isLocked;
		_fieldsManager.setField(FieldType.FIELD_LOCKED);
		
		return this;
	}
	
	public ForumInfo setForumPosition(int newPosition){
		_positionOfForum = newPosition;
		_fieldsManager.setField(FieldType.FIELD_POSITION);
		
		return this;
	}

	public ForumInfo setForumType(ForumType forumType){
		_forumType = forumType;
		_fieldsManager.setField(FieldType.FIELD_FORUM_TYPE);
		
		return this;
	}

	public ForumInfo setForumAccessType(ForumAccessType accessType){
		_forumAccsess = accessType;
		_fieldsManager.setField(FieldType.FIELD_FORUM_ACCESS_TYPE);
		
		return this;
	}

	public ForumInfo setParentCategoryId(int parentCategoryId){
		_parentCategoryId = parentCategoryId;
		_fieldsManager.setField(FieldType.FIELD_FORUM_PARENT_CATEGORY);
		
		return this;
	}
	
	
	
	@Override
	public String getJSONDescription() {
		List<NameValuePair> result = new LinkedList<NameValuePair>();
		
		if(_fieldsManager.isSeted(FieldType.FIELD_NAME))
			result.add(new BasicNameValuePair(FieldType.FIELD_NAME.getValue(), _forumName));
		if(_fieldsManager.isSeted(FieldType.FIELD_DESCRIPTION))
			result.add(new BasicNameValuePair(FieldType.FIELD_DESCRIPTION.getValue(), _forumDescription));
		if(_fieldsManager.isSeted(FieldType.FIELD_LOCKED))
			result.add(new BasicNameValuePair(FieldType.FIELD_LOCKED.getValue(), String.valueOf(_isLocked)));
		if(_fieldsManager.isSeted(FieldType.FIELD_POSITION))
			result.add(new BasicNameValuePair(FieldType.FIELD_POSITION.getValue(), String.valueOf(_positionOfForum)));
		if(_fieldsManager.isSeted(FieldType.FIELD_FORUM_TYPE))
			result.add(new BasicNameValuePair(FieldType.FIELD_FORUM_TYPE.getValue(), _forumType.getValue()));
		if(_fieldsManager.isSeted(FieldType.FIELD_FORUM_ACCESS_TYPE))
			result.add(new BasicNameValuePair(FieldType.FIELD_FORUM_ACCESS_TYPE.getValue(), _forumAccsess.getValue()));
		if(_fieldsManager.isSeted(FieldType.FIELD_FORUM_PARENT_CATEGORY))
			result.add(new BasicNameValuePair(FieldType.FIELD_FORUM_PARENT_CATEGORY.getValue(), String.valueOf(_parentCategoryId)));
		
		return getObjectJSONString(result);
	}

	
	
	
}
