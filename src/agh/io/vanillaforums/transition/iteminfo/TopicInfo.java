package agh.io.vanillaforums.transition.iteminfo;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.FieldType;


public class TopicInfo extends ContentItemInfo {

	static{
		__mandatoryFieldsSet = new HashSet<FieldType>();
		__updateFieldsSet = new HashSet<FieldType>();
		
		//setting mandatory fields
		__mandatoryFieldsSet.add(FieldType.FIELD_TOPIC_TITLE);
		__mandatoryFieldsSet.add(FieldType.FIELD_BODY);
		
		__updateFieldsSet.addAll(__mandatoryFieldsSet);
		//setting updateFields
		__updateFieldsSet.add(FieldType.FIELD_ID);
		
	}
	
	
	private String _topicTitle;
	private String _topicBody;
	private int _parentForumId;
	private boolean _isLocked;
	private int _position;

	
	public TopicInfo() {
		super(ContentType.TOPIC);
	}
	
	public TopicInfo setTopicTitle(String topicTitle) {
		_topicTitle = topicTitle;
		_fieldsManager.setField(FieldType.FIELD_TOPIC_TITLE);
		
		return this;
	}
	
	public TopicInfo setTopicBody(String topicBody) {
		_topicBody = topicBody;
		_fieldsManager.setField(FieldType.FIELD_BODY);
		
		return this;
	}

	public TopicInfo setParentForumId(int parentForumId) {
		_parentForumId = parentForumId;
		_fieldsManager.setField(FieldType.FIELD_PARENT_FORUM_ID);
		
		return this;
	}
	
	public TopicInfo setIsLocked(boolean isLocked) {
		_isLocked = isLocked;
		_fieldsManager.setField(FieldType.FIELD_LOCKED);
		
		return this;
	}
	
	public TopicInfo setPosition(int position) {
		_position = position;
		_fieldsManager.setField(FieldType.FIELD_POSITION);
		
		return this;
	}

	@Override
	public String  getJSONDescription() {
		List<NameValuePair> result = new LinkedList<NameValuePair>();
		
		if(_fieldsManager.isSeted(FieldType.FIELD_TOPIC_TITLE))
			result.add(new BasicNameValuePair(FieldType.FIELD_TOPIC_TITLE.getValue(), _topicTitle));
		if(_fieldsManager.isSeted(FieldType.FIELD_BODY))
			result.add(new BasicNameValuePair(FieldType.FIELD_BODY.getValue(), _topicBody));
		if(_fieldsManager.isSeted(FieldType.FIELD_PARENT_FORUM_ID))
			result.add(new BasicNameValuePair(FieldType.FIELD_PARENT_FORUM_ID.getValue(), String.valueOf(_parentForumId)));
		if(_fieldsManager.isSeted(FieldType.FIELD_LOCKED))
			result.add(new BasicNameValuePair(FieldType.FIELD_LOCKED.getValue(), String.valueOf(_isLocked)));
		if(_fieldsManager.isSeted(FieldType.FIELD_POSITION))
			result.add(new BasicNameValuePair(FieldType.FIELD_POSITION.getValue(), String.valueOf(_position)));
		
		return getObjectJSONString(result);
	}
	
	
	
	
}
