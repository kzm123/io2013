package agh.io.vanillaforums.transition.iteminfo;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.FieldType;
import agh.io.vanillaforums.transition.iteminfo.utils.UserRole;

public class UserInfo extends ContentItemInfo {

	static{
		__mandatoryFieldsSet = new HashSet<FieldType>();
		__mandatoryFieldsSet.add(FieldType.FIELD_NAME);
		__mandatoryFieldsSet.add(FieldType.FIELD_EMAIL);
		__mandatoryFieldsSet.add(FieldType.FIELD_ROLE);
		
		__updateFieldsSet = new HashSet<FieldType>();
		__updateFieldsSet.addAll(__mandatoryFieldsSet);
		__updateFieldsSet.add(FieldType.FIELD_ID);
	}


	private String _name;
	private String _alias;
	private String _email;
	private String _phone;
	private String _signature;
	private String _details;
	private UserRole _role;
	private boolean _isModerator;
	private boolean _isSuspended;
	
	public UserInfo() {
		super(ContentType.USER);
	}

	public UserInfo setUserName(String name){
		_fieldsManager.setField(FieldType.FIELD_NAME);
		_name = name;
		
		return this;
	}
	
	public UserInfo setUserAlias(String alias){
		_fieldsManager.setField(FieldType.FIELD_ALIAS);
		_alias = alias;
		
		return this;
	}
	
	public UserInfo setUserEmail(String email){
		_fieldsManager.setField(FieldType.FIELD_EMAIL);
		_email = email;
		
		return this;
	}
	
	public UserInfo setUserPhone(String phone){
		_fieldsManager.setField(FieldType.FIELD_PHONE);
		_phone = phone;
		
		return this;
	}
	
	public UserInfo setUserSignature(String signature){
		_fieldsManager.setField(FieldType.FIELD_SIGNATURE);
		_signature = signature;
		
		return this;
	}
	
	public UserInfo setUserDetails(String details){
		_fieldsManager.setField(FieldType.FIELD_DETAILS);
		_details = details;
		
		return this;
	}
	
	public UserInfo setUserRole(UserRole role){
		_fieldsManager.setField(FieldType.FIELD_ROLE);
		_role = role;
		
		return this;
	}
	
	public UserInfo setIsModerator(boolean isModerator){
		_fieldsManager.setField(FieldType.FIELD_MODERATOR);
		_isModerator = isModerator;
		
		return this;
	}
	
	public UserInfo setIsSuspended(boolean isSuspended){
		_fieldsManager.setField(FieldType.FIELD_SUSPENDED);
		_isSuspended = isSuspended;
		
		return this;
	}
	
	
	
	@Override
	public String getJSONDescription() {
		List<NameValuePair> result = new LinkedList<NameValuePair>();

		
		if(_fieldsManager.isSeted(FieldType.FIELD_NAME))
			result.add(new BasicNameValuePair(FieldType.FIELD_NAME.getValue(), _name));
		if(_fieldsManager.isSeted(FieldType.FIELD_ALIAS))
			result.add(new BasicNameValuePair(FieldType.FIELD_ALIAS.getValue(), _alias));
		if(_fieldsManager.isSeted(FieldType.FIELD_EMAIL))
			result.add(new BasicNameValuePair(FieldType.FIELD_EMAIL.getValue(), _email));
		if(_fieldsManager.isSeted(FieldType.FIELD_PHONE))
			result.add(new BasicNameValuePair(FieldType.FIELD_PHONE.getValue(), _phone));
		if(_fieldsManager.isSeted(FieldType.FIELD_SIGNATURE))
			result.add(new BasicNameValuePair(FieldType.FIELD_SIGNATURE.getValue(), _signature));
		if(_fieldsManager.isSeted(FieldType.FIELD_DETAILS))
			result.add(new BasicNameValuePair(FieldType.FIELD_DETAILS.getValue(), _details));
		if(_fieldsManager.isSeted(FieldType.FIELD_ROLE))
			result.add(new BasicNameValuePair(FieldType.FIELD_ROLE.getValue(), _role.getValue()));
		if(_fieldsManager.isSeted(FieldType.FIELD_MODERATOR))
			result.add(new BasicNameValuePair(FieldType.FIELD_MODERATOR.getValue(), String.valueOf(_isModerator)));
		if(_fieldsManager.isSeted(FieldType.FIELD_SUSPENDED))
			result.add(new BasicNameValuePair(FieldType.FIELD_SUSPENDED.getValue(), String.valueOf(_isSuspended)));
			
		return getObjectJSONString(result);
	}

	
	
	
	
	
	
	
	
	
}
