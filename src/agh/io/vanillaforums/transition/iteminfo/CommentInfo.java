package agh.io.vanillaforums.transition.iteminfo;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.FieldType;

public class CommentInfo extends ContentItemInfo{

	static{
		__mandatoryFieldsSet = new HashSet<FieldType>();
		__updateFieldsSet = new HashSet<FieldType>();
		
		//setting mandatory fields
		__mandatoryFieldsSet.add(FieldType.FIELD_TOPIC_ID);
		__mandatoryFieldsSet.add(FieldType.FIELD_BODY);
		//setting update fields
		__updateFieldsSet.addAll(__mandatoryFieldsSet);
		__updateFieldsSet.add(FieldType.FIELD_ID);
	}
	
	private int _topic_id;
	private int _creator_id;
	private String _commentBody;
	

	public CommentInfo() {
		super(ContentType.COMMENT);
	}
	
	
	public CommentInfo setTopicId(int topic_id) {
		_topic_id = topic_id;
		_fieldsManager.setField(FieldType.FIELD_TOPIC_ID);
		
		return this;
	}



	public CommentInfo setCreatorId(int creator_id) {
		_creator_id = creator_id;
		_fieldsManager.setField(FieldType.FIELD_CREATOR_ID);
	
		return this;
	}



	public CommentInfo setCommentBody(String commentBody) {
		_commentBody = commentBody;
		_fieldsManager.setField(FieldType.FIELD_BODY);
	
		return this;
	}



	@Override
	public String getJSONDescription() {
		List<NameValuePair> result = new LinkedList<NameValuePair>();
		
		if(_fieldsManager.isSeted(FieldType.FIELD_TOPIC_ID))
			result.add(new BasicNameValuePair(FieldType.FIELD_TOPIC_ID.getValue(), String.valueOf(_topic_id)));
		if(_fieldsManager.isSeted(FieldType.FIELD_CREATOR_ID))
			result.add(new BasicNameValuePair(FieldType.FIELD_CREATOR_ID.getValue(), String.valueOf(_creator_id)));
		if(_fieldsManager.isSeted(FieldType.FIELD_BODY))
			result.add(new BasicNameValuePair(FieldType.FIELD_BODY.getValue(), _commentBody));
		
		return getObjectJSONString(result);
	}

}
