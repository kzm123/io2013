package agh.io.vanillaforums.transition.iteminfo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.FieldType;
import agh.io.vanillaforums.transition.iteminfo.utils.FieldsManager;

public abstract class ContentItemInfo {
	
	protected static Set<FieldType> __mandatoryFieldsSet;
	protected static Set<FieldType> __updateFieldsSet;
	
	protected ContentType _typeOfContent;
	protected int _id;
	protected FieldsManager _fieldsManager = new FieldsManager();
	
	public ContentItemInfo(ContentType typeOfMe){
		_typeOfContent = typeOfMe;
	}
	
	public int getId(){
		return _id;
	}
	
	public ContentItemInfo setId(int id){
		_id = id;
		_fieldsManager.setField(FieldType.FIELD_ID);
		
		return this;
	}
	
	public boolean mandatoryFieldsSeted() {
		for(FieldType ft : __mandatoryFieldsSet)
			if(_fieldsManager.isSeted(ft) == false)
				return false;
		
		return true;
	}

	public Set<FieldType> getUnsetedMandatoryFields(){
		Set<FieldType> result = new HashSet<FieldType>();
		
		for(FieldType ft : __mandatoryFieldsSet)
			if(_fieldsManager.isSeted(ft) == false)
				result.add(ft);
		
		return result;
	}
	
	public boolean updateFieldSeted() {
		for(FieldType ft : __updateFieldsSet)
			if(_fieldsManager.isSeted(ft) == false)
				return false;
		return true;
	}
	
	public Set<FieldType> getUnsetedUpdateFields(){
		Set<FieldType> result = new HashSet<FieldType>();
		
		for(FieldType ft : __updateFieldsSet)
			if(_fieldsManager.isSeted(ft) == false)
				result.add(ft);
		
		return result;
	}
	
	protected String getObjectJSONString (List<NameValuePair> attributes){
		String result = null;
		
		
		try {
			JSONObject innerObj = new JSONObject();	
			for(NameValuePair np : attributes){
					innerObj.put(np.getName(), np.getValue());
			}
			
			JSONObject outerObj = new JSONObject();
			outerObj.put(_typeOfContent.getValue(),innerObj);
			
			result = outerObj.toString();
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public abstract String getJSONDescription();

}
