package agh.io.vanillaforums.transition.iteminfo;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.FieldType;



public class CategoryInfo extends ContentItemInfo {

	static{
		__mandatoryFieldsSet = new HashSet<FieldType>();
		__mandatoryFieldsSet.add(FieldType.FIELD_NAME);
		
		__updateFieldsSet = new HashSet<FieldType>();
		__updateFieldsSet.addAll(__mandatoryFieldsSet);
		__updateFieldsSet.add(FieldType.FIELD_ID);
	}
	
	
	private String _categoryName = null;
	private String _description = null;
	private int _position = 0;
	

	public CategoryInfo(){
		super(ContentType.CATEGORY);
	}
	
	public CategoryInfo setCategoryName(String categoryName){
		_categoryName = categoryName;
		_fieldsManager.setField(FieldType.FIELD_NAME);
		
		return this;
	}
	
	public CategoryInfo setCategoryDescription(String categoryDescription){
		_description = categoryDescription;
		_fieldsManager.setField(FieldType.FIELD_DESCRIPTION);
		
		return this;
	}
	
	public CategoryInfo setPosition(int position){
		_position = position;
		_fieldsManager.setField(FieldType.FIELD_POSITION);
		
		return this;
	}
	
	@Override
	public String getJSONDescription() {
		List<NameValuePair> result = new LinkedList<NameValuePair>();
		
		if(_fieldsManager.isSeted(FieldType.FIELD_NAME))
			result.add(new BasicNameValuePair(FieldType.FIELD_NAME.getValue(), _categoryName));
		if(_fieldsManager.isSeted(FieldType.FIELD_DESCRIPTION))
			result.add(new BasicNameValuePair(FieldType.FIELD_DESCRIPTION.getValue(), _description));
		if(_fieldsManager.isSeted(FieldType.FIELD_POSITION))
			result.add(new BasicNameValuePair(FieldType.FIELD_POSITION.getValue(), String.valueOf(_position)));
		
		return getObjectJSONString(result);
	}
	
	
		
}
