package agh.io.vanillaforums.transition.testing;

@SuppressWarnings("rawtypes")
public class MethodInfo {
	
	public Class [] params_types;
	public int params_count;
	
	public MethodInfo(Class ... paramTypes){
		params_types = paramTypes;
		params_count = params_types.length;
	}
}