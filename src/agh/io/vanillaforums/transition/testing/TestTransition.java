package agh.io.vanillaforums.transition.testing;


import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;
import java.util.TreeMap;

import agh.io.vanillaforums.R;
import agh.io.vanillaforums.transition.ContentRemoteManager;
import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.IFlatCategory;
import agh.io.vanillaforums.transition.interfaces.items.IFlatComment;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForumSubscription;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopicSubscription;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.AlreadyExistsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.CategoryInfo;
import agh.io.vanillaforums.transition.iteminfo.CommentInfo;
import agh.io.vanillaforums.transition.iteminfo.ForumInfo;
import agh.io.vanillaforums.transition.iteminfo.TopicInfo;
import agh.io.vanillaforums.transition.iteminfo.UserInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.UserRole;
import agh.io.vanillaforums.transition.utils.Constants;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;




public class TestTransition extends Activity {

	
	private Button executeB;
	private Button clearB;
	private TextView tv;
	
	private IContentRemoteManager _remoteManager;
	private Method toExecute;
	private Object[] params;
	
	/*
	 *  Test methods
	 */
	
	public void getAllCategoriesTest() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		List<IFlatCategory> categories;
		categories = _remoteManager.getAllCategories();
		StringBuffer buf = new StringBuffer("Categories on service : \n");
		
		for(IFlatCategory cat : categories)
			buf.append(cat.toString());
		
		tv.setText(buf.toString());
	}
	public void getCategoryTest(int categoryId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		IFlatCategory result = _remoteManager.getCategory(categoryId);
		
		tv.setText("Requested category " + result.toString());
}
	public void createCategoryTest(CategoryInfo info) throws NotSetedMandatoryFieldsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, AlreadyExistsException{
		IFlatCategory result = _remoteManager.createCategory(info);
		tv.setText(result.toString());
	}
	public void updateCategoryTest(int categoryId, CategoryInfo info) throws NotAuthorizedException, NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException{
		IFlatCategory updated = _remoteManager.updateCategory(categoryId, info);
		tv.setText(updated.toString());
	}
	public void deleteCategoryTest(int categoryId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		boolean result = _remoteManager.deleteCategory(categoryId);
		tv.setText(String.valueOf(result));
	}
	
	public void getAllForumsTest() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException{
		List<IFlatForum> result = _remoteManager.getAllForums();
		StringBuffer sb = new StringBuffer("Avaliable forums : \n\n");
		
		for(IFlatForum f : result){
			sb.append(f.toString());
		}
		
		tv.setText(sb.toString());
	}
	public void getCategoryForumsTest(int categoryId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException{
		List<IFlatForum> result = _remoteManager.getCategoryForums(categoryId);
		StringBuffer sb = new StringBuffer("Avaliable forums : \n\n");
		
		for(IFlatForum f : result){
			sb.append(f.toString());
		}
		
		tv.setText(sb.toString());
	}	
	public void getForumTest(int forumId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException{
		IFlatForum result = _remoteManager.getForum(forumId);
		tv.setText(result.toString());
	}
	public void createForumTest(ForumInfo info) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, AlreadyExistsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException{
		IFlatForum result = _remoteManager.createForum(info);
		tv.setText(result.toString());
	}	
	public void updateForumTest(int forumId, ForumInfo info) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		IFlatForum result = _remoteManager.updateForum(forumId, info);
		tv.setText(result.toString());
	}
	public void deleteForumTest(int forumId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		boolean result = _remoteManager.deleteForum(forumId);
		tv.setText(String.valueOf(result));
	}	
	
	
	public void getAllTopicsTest() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException{
		List<IFlatTopic> result = _remoteManager.getAllTopics();
		
		StringBuffer sb = new StringBuffer("Avaliable topics :: \n\n");
		
		for(IFlatTopic ft : result)
			sb.append(ft.toString());
		
		tv.setText(sb.toString());
	}
	public void getForumTopicsTest(int forumId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		List<IFlatTopic> result = _remoteManager.getForumTopics(forumId);
		
		StringBuffer sb = new StringBuffer("Avaliable topics :: \n\n");
		
		for(IFlatTopic ft : result)
			sb.append(ft.toString());
		
		tv.setText(sb.toString());
	}
	public void getUsersTopicsTest(int userId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		List<IFlatTopic> result = _remoteManager.getUserTopics(userId);
		
		StringBuffer sb = new StringBuffer("Avaliable topics :: \n\n");
		
		for(IFlatTopic ft : result)
			sb.append(ft.toString());
		
		tv.setText(sb.toString());
	}
	public void getTopicTest(int topicId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		IFlatTopic result = _remoteManager.getTopic(topicId);
		tv.setText(result.toString());
	}
	public void createTopicTest(TopicInfo info) throws NotSetedMandatoryFieldsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, AlreadyExistsException, InvalidRecordException{
		IFlatTopic result = _remoteManager.createTopic(info);
		tv.setText(result.toString());
	}
	public void updateTopicTest(int topicId, TopicInfo info) throws RemoteObjectDoesNotExistException, NotSetedMandatoryFieldsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		IFlatTopic result = _remoteManager.updateTopic(topicId, info);
		tv.setText(result.toString());
	}
	public void deleteTopicTest(int topicId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		boolean result = _remoteManager.deleteTopic(topicId);
		tv.setText(String.valueOf(result));
	}
	
	
	public void getTopicCommentsTest(int topicId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		List<IFlatComment> result = _remoteManager.getTopicComments(topicId);
		
		StringBuffer sb = new StringBuffer("avaliable comments ::\n\n");
		
		for(IFlatComment c : result)
			sb.append(c.toString());
		
		tv.setText(sb.toString());
	}
	public void getUsersCommentsTest(int userId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		List<IFlatComment> result = _remoteManager.getUserComments(userId);
		
		StringBuffer sb = new StringBuffer("avaliable comments ::\n\n");
		
		for(IFlatComment c : result)
			sb.append(c.toString());
		
		tv.setText(sb.toString());
	}
	public void getTopicCommentTest(int topicId, int commentId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		IFlatComment result = _remoteManager.getTopicComment(topicId, commentId);
		tv.setText(result.toString());
	}
	public void getUserCommentTest(int userId, int commentId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		IFlatComment result = _remoteManager.getUserComment(userId, commentId);
		tv.setText(result.toString());
	}
	public void createUserCommentTest(int topicId, CommentInfo info) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, AlreadyExistsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		IFlatComment result = _remoteManager.createComment(topicId, info);
		tv.setText(result.toString());
	}
	public void updateCommentTest(int topicId, int commentId, CommentInfo info) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		IFlatComment result = _remoteManager.updateComment(topicId, commentId, info);
		tv.setText(result.toString());
	}
	public void deleteCommentTest(int topicId, int commentId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		boolean result = _remoteManager.deleteComment(topicId, commentId);
		tv.setText(String.valueOf(result));
	}
	
	public void getAllTopicSubscriptionsTest() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		List<IFlatTopicSubscription> result = _remoteManager.getAllTopicSubscriptions();
		StringBuffer sb = new StringBuffer();
		sb.append("Topic Subscriptions ::\n");
		for(IFlatTopicSubscription s : result)
			sb.append(s.toString());
		
		tv.setText(sb.toString());
	}
	
	public void getTopicSubscriptionsTest(int topicId) throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		List<IFlatTopicSubscription> result = _remoteManager.getTopicSubscriptions(topicId);
		StringBuffer sb = new StringBuffer();
		sb.append("Topic Subscriptions ::\n");
		for(IFlatTopicSubscription s : result)
			sb.append(s.toString());
		
		tv.setText(sb.toString());
	}
	
	public void getTopicSubscriptionTest(int subscriptionId) throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		IFlatTopicSubscription result = _remoteManager.getTopicSubscription(subscriptionId);
		tv.setText(result.toString());
	}
	
	
	public void createTopicSubscriptionTest(int topicId, int userId) throws UnsupportedEncodingException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException{
		IFlatTopicSubscription result = _remoteManager.createTopicSubscription(topicId, userId);
		tv.setText(result.toString());
	}
	
	public void deleteTopicSubscriptionTest(int subscriptionId) throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, RemoteObjectDoesNotExistException{
		tv.setText(String.valueOf(_remoteManager.deleteTopicSubscription(subscriptionId)));
	}
	
	
	
	public void getAllForumSubscriptionsTest() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		List<IFlatForumSubscription> result = _remoteManager.getAllForumSubscriptions();
		StringBuffer sb = new StringBuffer();
		sb.append("Forum Subscriptions ::\n");
		for(IFlatForumSubscription s : result)
			sb.append(s.toString());
		
		tv.setText(sb.toString());
	}
	
	
	public void showForumSubscriptionTest(int subscriptionId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		IFlatForumSubscription result = _remoteManager.getForumSubscription(subscriptionId);
		tv.setText(result.toString());
	}
	
	
	public void deleteForumSubscriptionTest(int subscriptionId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		tv.setText(String.valueOf(_remoteManager.deleteForumSubscription(subscriptionId)));
	}
	
	public void createForumSubscriptionTest(int forumId,int userId) throws UnsupportedEncodingException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException{
		IFlatForumSubscription result = _remoteManager.createForumSubscription(forumId, userId);
		tv.setText(result.toString());
	}
	
	public void getUserRelatedInfoTest(int userId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		tv.setText(_remoteManager.getUserRelatedInfo(userId).toString());
	}
	
	public void getAllUsersTest() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException{
		StringBuffer sb = new StringBuffer("All users ::\n\n");
		for(IFlatUser u: _remoteManager.getAllUsers())
			sb.append(u.toString());
		
		tv.setText(sb.toString());
	}
	
	public void getUserTest(int userId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		tv.setText(_remoteManager.getUser(userId).toString());
	}
	
	public void createUserTest(UserInfo info) throws NotSetedMandatoryFieldsException, AlreadyExistsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException{
		tv.setText(_remoteManager.createUser(info).toString());
	}
	
	public void deleteuserTest(int userId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		tv.setText(String.valueOf(_remoteManager.deleteUser(userId)));
	}
	
	public void updateUserTest(int userId, UserInfo info) throws UnsupportedEncodingException, NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException{
		tv.setText(_remoteManager.updateUser(userId, info).toString());
	}
	
	public void setUserPasswordTest(int userId,String password) throws UnsupportedEncodingException, RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		tv.setText(String.valueOf(_remoteManager.setUserPassword(userId, password)));
	}
	
	public void changeUserPasswordTest(int userId, String oldPassword, String newPassword) throws UnsupportedEncodingException, RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		tv.setText(String.valueOf(_remoteManager.changeUserPassword(userId, oldPassword, newPassword)));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public void requestSandBox(Method m, Object ... params){
		try {
			
			m.invoke(this, params);
			
		}catch(InvocationTargetException e){
			e.printStackTrace();
			tv.setText(e.getTargetException().toString());
		}catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
	
	//===============================================
	//				METHODS names
	private static final String getAllCategories = "getAllCategoriesTest";			// 0 params
	private static final String getCategory = "getCategoryTest"; 					// 1 param - int (categoryId)
	private static final String createCategory = "createCategoryTest";				// 1 param - CategoryInfo
	private static final String updateCategory = "updateCategoryTest";				// 2 params - int (categoryId) , CategoryInfo
	private static final String deleteCategory = "deleteCategoryTest";				// 1 param - int (categoryId)
	
	private static final String getAllForums = "getAllForumsTest";					// 0 params
	private static final String getCategoryForums = "getCategoryForumsTest";		// 1 param - int (categoryId)
	private static final String getForum = "getForumTest";							// 1 param - int (forumId) 
	private static final String createForum = "createForumTest";					// 1 param - ForumInfo
	private static final String updateForum = "updateForumTest";					// 2 param - int (forumId), ForumInfo
	private static final String deleteForum = "deleteForumTest";					// 1 param - int (forumId)
	
	private static final String getAllTopics = "getAllTopicsTest";					// 0 params
	private static final String getForumTopics = "getForumTopicsTest";				// 1 param - int (forumId)
	private static final String getUsersTopics = "getUsersTopicsTest";				// 1 param - int (userId)
	private static final String getTopic = "getTopicTest";							// 1 param - int (topicId)
	private static final String createTopic = "createTopicTest";					// 1 param - TopicInfo
	private static final String updateTopic = "updateTopicTest";					// 2 params - int (topicId), TopicInfo
	private static final String deleteTopic = "deleteTopicTest";					// 1 param - int (topicId)
	
	
	private static final String getTopicComments = "getTopicCommentsTest";			// 1 param - int (topicId)
	private static final String getUserComments = "getUsersCommentsTest";			// 1 param - int (userId)
	private static final String getTopicComment = "getTopicCommentTest";			// 2 params - int (topicId), int (commentId)
	private static final String getUserComment = "getUserCommentTest";				// 2 params - int (userId), int (commentId)
	private static final String createComment = "createUserCommentTest";			// 2 params - int (topicId), CommentInfo
	private static final String updateComment = "updateCommentTest";				// 3 params - int (topicId), int (commentId), CommentInfo
	private static final String deleteComment = "deleteCommentTest";				// 2 params - int (topicId), int (commentId)
	
	
	private static final String getAllTopicSubscriptions = "getAllTopicSubscriptionsTest";	// 0 params
	private static final String getTopicSubscriptions = "getTopicSubscriptionsTest"; 		//1 param - int (topicId) 
	private static final String getTopicSubscription = "getTopicSubscriptionTest";			// 1 param - int (subscriptionId)
	private static final String createTopicSubscription = "createTopicSubscriptionTest";	// 2 params - int(topicId), int (userId)
	private static final String deleteTopicSubscription = "deleteTopicSubscriptionTest";	// 1 param - int(subscriptionId)
	
	private static final String getAllForumSubscriptions = "getAllForumSubscriptionsTest";	// 0 params
	private static final String getForumSubscriptions = "getForumSubscriptionsTest";		// 1 param - int (forumId)
	private static final String showForumSubscription = "showForumSubscriptionTest";		// 1 param - int (subscriptionId)
	private static final String deleteForumSubscription = "deleteForumSubscriptionTest";	// 1 param - int (subscriptionId)
	private static final String createForumSubscription = "createForumSubscriptionTest";	// 2 params - int (forumId), int (userId)
	
	private static final String getUserRealatedInfo = "getUserRelatedInfoTest";				// 1 param	- int (userId)
	
	private static final String getAllUsers	= "getAllUsersTest";							// 0 params
	private static final String getUser = "getUserTest";									// 1 param - int (userId)
	private static final String createUser = "createUserTest";								// 1 param - UserInfo
	private static final String deleteUser = "deleteuserTest";								// 1 param - int (userId)
	private static final String updateUser = "updateUserTest";								// 2 params - int (userId), UserInfo
	private static final String setUserPassword = "setUserPasswordTest";					// 2 params - int userId, String password
	private static final String changeUserPassword = "changeUserPasswordTest";				// 3 params - int (userId), String - oldPass, String new pass
	
	
	private static TreeMap<String, MethodInfo> __methodsInfo = new TreeMap<String, MethodInfo>();
	static{
		MethodInfo m;
		
		m = new MethodInfo();
		__methodsInfo.put(getAllCategories, m);
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(getCategory,m);
		//=====================================
		m = new MethodInfo(CategoryInfo.class);
		__methodsInfo.put(createCategory,m);
		//=====================================
		m = new MethodInfo(int.class,CategoryInfo.class);
		__methodsInfo.put(updateCategory, m);
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(deleteCategory,m);
		
		
		//=====================================
		m = new MethodInfo();
		__methodsInfo.put(getAllForums, m);
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(getCategoryForums, m);
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(getForum, m);
		//=====================================
		m = new MethodInfo(ForumInfo.class);
		__methodsInfo.put(createForum,m);
		//=====================================
		m = new MethodInfo(int.class,ForumInfo.class);
		__methodsInfo.put(updateForum,m);
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(deleteForum, m);
	
	
		
		//=====================================
		m = new MethodInfo();
		__methodsInfo.put(getAllTopics, m);
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(getForumTopics, m);
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(getUsersTopics, m);
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(getTopic, m);
		//=====================================
		m = new MethodInfo(TopicInfo.class);
		__methodsInfo.put(createTopic, m);
		//=====================================
		m = new MethodInfo(int.class,TopicInfo.class);
		__methodsInfo.put(updateTopic,m);
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(deleteTopic,m);
		//=====================================
		
		
		
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(getTopicComments, m);
		//=====================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(getUserComments, m);
		//=====================================
		m = new MethodInfo(int.class,int.class);
		__methodsInfo.put(getTopicComment, m);
		//=====================================
		m = new MethodInfo(int.class,int.class);
		__methodsInfo.put(getUserComment, m);
		//=====================================
		m = new MethodInfo(int.class,CommentInfo.class);
		__methodsInfo.put(createComment,m);
		//=====================================
		m = new MethodInfo(int.class,int.class,CommentInfo.class);
		__methodsInfo.put(updateComment,m);
		//=====================================
		m = new MethodInfo(int.class,int.class);
		__methodsInfo.put(deleteComment, m);
		
		//=====================================
		m = new MethodInfo();
		__methodsInfo.put(getAllTopicSubscriptions, m);
		//======================================
		m = new MethodInfo(int.class);
		__methodsInfo.put(getTopicSubscriptions, m);
		//======================================
		__methodsInfo.put(getTopicSubscription, new MethodInfo(int.class));
		//======================================
		__methodsInfo.put(createTopicSubscription, new MethodInfo(int.class,int.class));
		//======================================
		__methodsInfo.put(deleteTopicSubscription, new MethodInfo(int.class));
		
		//======================================
		__methodsInfo.put(getAllForumSubscriptions,new MethodInfo());
		__methodsInfo.put(getForumSubscriptions, new MethodInfo(int.class));
		__methodsInfo.put(showForumSubscription, new MethodInfo(int.class));
		__methodsInfo.put(deleteForumSubscription, new MethodInfo(int.class));
		__methodsInfo.put(createForumSubscription, new MethodInfo(int.class,int.class));
		
		
		
		__methodsInfo.put(getUserRealatedInfo, new MethodInfo(int.class));
		__methodsInfo.put(getAllUsers,new MethodInfo());
		__methodsInfo.put(getUser,new MethodInfo(int.class));
		__methodsInfo.put(createUser,new MethodInfo(UserInfo.class));
		__methodsInfo.put(deleteUser,new MethodInfo(int.class));
		__methodsInfo.put(updateUser,new MethodInfo(int.class,UserInfo.class));
		__methodsInfo.put(setUserPassword,new MethodInfo(int.class,String.class));
		__methodsInfo.put(changeUserPassword, new MethodInfo(int.class,String.class,String.class));
	}
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test_transition);
		
		tv = (TextView) findViewById(R.id.getReqText);
		executeB = (Button) findViewById(R.id.buttExecute);
		clearB = (Button) findViewById(R.id.buttClear);
		
		_remoteManager = ContentRemoteManager.getDefaultInstance();
		
		
		_remoteManager.logIn(Constants.API_USERNAME, Constants.API_PASSWORD);
		
		
		
		//===============================================
		//				Control
		String currentTest = setUserPassword;
		
		MethodInfo mi = __methodsInfo.get(currentTest);
		int paramsCount = mi.params_count;
		@SuppressWarnings("rawtypes")
		Class [] paramsTypes = mi.params_types;
		
		params = new Object[paramsCount];
		
		//	setting up params of invocation
//		CategoryInfo catinfo = new CategoryInfo();
		UserInfo userInfo = new UserInfo();
			userInfo.setUserName("StudyAgent")
						.setUserAlias("pizdaliz")
						.setUserEmail("agent@study.zendesk")
						.setUserRole(UserRole.AGENT)
						.setUserPhone("134-234-221")
						.setUserDetails("Moze zmieniac hasla")
						.setIsModerator(true)
						.setId(404416313);
		params[0] = 404439873;
		params[1] = "dupos";
//		params[1] = "dupa";
	//	params[] = userInfo;	//userId
//		catinfo	.setCategoryName("Kategoria ostatnia")
	//			.setCategoryDescription("Zmienilem to za pomoca API-")
		//		.setPosition(2);
//		
//		ForumInfo fInfo = new ForumInfo();
//		fInfo	.setForumName("Dupoforum (ZMIENIONA)")
//				.setForumDescription("Dodany opis forum")
//				.setForumAccessType(ForumAccessType.ACC_EVERYBODY)
//				.setForumPosition(1)
//				.setParentCategoryId(20107657);
//		params[1] = fInfo;
	
//		TopicInfo ti = new TopicInfo();
//		params[0] = 25972213;
//		subscription id= 26285167
//		ti	.setParentForumId(22109581)
//			.setTopicTitle("Nazwa topiku zmieniona")
//			.setTopicBody("Topic body TO (ZMIENIONY)")
//			.setPosition(1);
//	topic id to test = 24078227
		
//		CommentInfo info = new CommentInfo();
//		
//		info	.setCommentBody("<qoute> DUPSKO </quote> edit : nowa zawartosc");
		//params[2] = info;
		
		//===============================================
		
		try {
			toExecute = TestTransition.class.getMethod(currentTest, paramsTypes);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		
		clearB.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				tv.setText("");
			}
		});
		
		executeB.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				requestSandBox(toExecute, params);	
			}
		});
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.test_transition, menu);
		return true;
	}

}
