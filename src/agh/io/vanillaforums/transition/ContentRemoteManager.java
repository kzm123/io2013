package agh.io.vanillaforums.transition;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.impl.FlatUserRelatedInfo;
import agh.io.vanillaforums.transition.impl.RequestManager;
import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.items.IFlatCategory;
import agh.io.vanillaforums.transition.interfaces.items.IFlatComment;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForumSubscription;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopicSubscription;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;
import agh.io.vanillaforums.transition.interfaces.managing.IRequestManager;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.AlreadyExistsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.CategoryInfo;
import agh.io.vanillaforums.transition.iteminfo.CommentInfo;
import agh.io.vanillaforums.transition.iteminfo.ContentItemInfo;
import agh.io.vanillaforums.transition.iteminfo.ForumInfo;
import agh.io.vanillaforums.transition.iteminfo.TopicInfo;
import agh.io.vanillaforums.transition.iteminfo.UserInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.FieldType;
import agh.io.vanillaforums.transition.utils.Constants;
import agh.io.vanillaforums.transition.utils.IAPIUrlBuilder;
import agh.io.vanillaforums.transition.utils.ZendeskAPIUrlBuilder;
import agh.io.vanillaforums.transition.utils.ZendeskContentJSONIntepreters;
import agh.io.vanillaforums.transition.utils.ZendeskJSONForumDataParser;



public class ContentRemoteManager implements IContentRemoteManager {

	private IRequestManager _requestManager;
	private IJSONForumDataParser _forumDataParser;
	private IAPIUrlBuilder _urlBuilder;
	private String _userName;
	private String _password;
	private boolean _isLogedIn;
	
	private ContentRemoteManager(IRequestManager reqManager, IJSONForumDataParser forumDataParser, IAPIUrlBuilder urlBuilder){
		_requestManager = reqManager;
		_forumDataParser = forumDataParser;
		_urlBuilder = urlBuilder;
	}
	
	public static IContentRemoteManager __instance = null;
	
	public static IContentRemoteManager getDefaultInstance(){
		if(__instance == null){
			IJSONForumDataParser dataParser = new ZendeskJSONForumDataParser();
			__instance = new ContentRemoteManager(new RequestManager(), dataParser, new ZendeskAPIUrlBuilder());
			dataParser.setContentRemoteManager(__instance);
		}
		
		return __instance;
	}
	
	
	private HttpRequestBase injectCredentials(HttpRequestBase baseReqest){
		if(_isLogedIn){
			baseReqest.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(_userName, _password), "UTF-8", false));
		}
		
		return baseReqest;
	}
	private HttpEntityEnclosingRequestBase injectContentInfo(HttpEntityEnclosingRequestBase request, ContentItemInfo information) throws UnsupportedEncodingException{
		String jsonDescription = information.getJSONDescription();
		request.setEntity(new StringEntity(jsonDescription));
		request.setHeader(Constants.SERVICE_API_HEADER_ACCEPT, Constants.SERVICE_API_HEADER_CONTENT_TYPE_VALUE);
		request.setHeader(Constants.SERVICE_API_HEADER_CONTENT_TYPE, Constants.SERVICE_API_HEADER_CONTENT_TYPE_VALUE);
		
		return request;
	}
	
	private String  processResponse(HttpResponse response, String url) 
			throws NotAuthorizedException, BadRequestException, 
			UnprocessableEntityException, ServiceUnavaliableException, 
			NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException{
		
		
		int statusCode = response.getStatusLine().getStatusCode();
		String result = null;
		ByteArrayOutputStream o = null;
		try {
			o = new ByteArrayOutputStream();
			response.getEntity().writeTo(o);
			result = o.toString();
		} catch (IOException e1) {
			e1.printStackTrace();
		}finally{
			if(o != null)
				try {
					o.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
		}

System.out.println("\tRequest url : " + url);		
System.out.println("\tResponse code : " + statusCode + " response : " + result);
		
		switch(statusCode){
		case  Constants.SERVICE_API_RESPONSE_UNAUTHORISED :{
			throw new NotAuthorizedException();
		}
		case Constants.SERVICE_API_RESPONSE_BAD_REQUEST :{
			throw new BadRequestException(url);
		}
		case Constants.SERVICE_API_RESPONSE_INTERNAL_ERROR :{
			throw new InternalServerErrorException();
		}
		case Constants.SERVICE_API_RESPONSE_NOT_IMPLEMENTED :{
			throw new NotImplementedException();
		}
		case Constants.SERVICE_API_RESPONSE_SERVICE_UNAVALIABLE :{
			throw new ServiceUnavaliableException();
		}
		case Constants.SERVICE_API_RESPONSE_UNPROCESSABLE_ENTITY :{
			throw new UnprocessableEntityException();
		}
		case Constants.SERVICE_API_RESPONSE_NOT_FOUND :{
			throw new NotFoundException();
		}
		case Constants.SERVICE_API_RESPONSE_INVALID_RECORD :{
			try {
				throw new InvalidRecordException(new JSONObject(result));
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		default :{	
			return result;
		}
		}
		
	}
	
	
	private String excecuteGETRequest(String getUrl) throws NotAuthorizedException, BadRequestException,
					UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, 
					InternalServerErrorException, NotFoundException, InvalidRecordException{
		
		HttpGet request =  new HttpGet(getUrl);
		request = (HttpGet) injectCredentials(request);
		String result = null;
		try {
			
			HttpResponse response = _requestManager.executeRequest(request);
			result = processResponse(response, getUrl);
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	private HttpResponse excecuteDELETERequest(String deleteUrl) throws NotAuthorizedException, BadRequestException,
				UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException,
				InternalServerErrorException, NotFoundException, InvalidRecordException{
		
		HttpDelete request =  new HttpDelete(deleteUrl);
		request = (HttpDelete) injectCredentials(request);
		HttpResponse response = null;
		
		try {
			
			response = _requestManager.executeRequest(request);
			processResponse(response, deleteUrl);
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}
	

	private HttpResponse getResponsePOSTRequest(String postUrl,	ContentItemInfo contentItemInfo) throws UnsupportedEncodingException {
		HttpPost request = new HttpPost(postUrl);
		request = (HttpPost) injectCredentials(request);
		request = (HttpPost) injectContentInfo(request, contentItemInfo);
			
		HttpResponse response = null;
		try {
			response = _requestManager.executeRequest(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return response;
	}
	
	
	
	private String excecutePOSTRequest(String postUrl, ContentItemInfo itemInfo) throws NotAuthorizedException, BadRequestException,
			UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, 
			InternalServerErrorException, NotFoundException, InvalidRecordException, UnsupportedEncodingException{
		
		HttpResponse response = getResponsePOSTRequest(postUrl, itemInfo);
		String responseString = processResponse(response, postUrl);
		
		return responseString;
	}
		

	private HttpResponse getResponsePUTRequest(String putUrl, ContentItemInfo contentInfo) throws UnsupportedEncodingException{
		HttpPut request = new HttpPut(putUrl);
		request = (HttpPut) injectCredentials(request);
		request = (HttpPut) injectContentInfo(request, contentInfo);
		
		HttpResponse response = null;
		try {
			response = _requestManager.executeRequest(request);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return response;
	}

	private String excecutePUTRequest(String putUrl, ContentItemInfo itemInfo) throws NotAuthorizedException, BadRequestException,
			UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, 
			InternalServerErrorException, NotFoundException, InvalidRecordException, UnsupportedEncodingException{
		
		HttpResponse response = getResponsePUTRequest(putUrl, itemInfo);
		String responseString = processResponse(response, putUrl);
		
		return responseString;
	}
	
	private void checkUpdateFields(ContentItemInfo contentInfo) throws NotSetedMandatoryFieldsException{
		if(contentInfo.updateFieldSeted() == false){
			NotSetedMandatoryFieldsException e = new NotSetedMandatoryFieldsException(ContentType.CATEGORY);
			
			for(FieldType ft : contentInfo.getUnsetedUpdateFields()){
				e.addUsetedField(ft);
			}
			
			throw e;
		}
	}
	
	private void checkMandatoryFields(ContentItemInfo contentInfo) throws NotSetedMandatoryFieldsException{
		if(contentInfo.mandatoryFieldsSeted() == false){
			NotSetedMandatoryFieldsException e = new NotSetedMandatoryFieldsException(ContentType.CATEGORY);
			
			for(FieldType ft : contentInfo.getUnsetedMandatoryFields()){
				e.addUsetedField(ft);
			}
			
			throw e;
		}
	}
	
	
	@Override
	public List<IFlatCategory> getAllCategories() 
			throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, 
			ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		String getUrl = _urlBuilder.listCategoriesUrl();
		List<IFlatCategory> resultList = new LinkedList<IFlatCategory>();
		
		try {	
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.CATEGORY_INT.getValue().getListOfItems(responseString, _forumDataParser))
				resultList.add((IFlatCategory) item);

		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}			
		
		return resultList;
	}

	@Override
	public IFlatCategory getCategory(int categoryId)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, 
			BadRequestException, UnprocessableEntityException, 
			ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {

		String getUrl = _urlBuilder.showCategoryUrl(categoryId);
		IFlatCategory result = null;
		
		try {

			String responseString = excecuteGETRequest(getUrl);
			result = (IFlatCategory) ZendeskContentJSONIntepreters.CATEGORY_INT.getValue().getItem(responseString, _forumDataParser);
			
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.CATEGORY, categoryId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public IFlatCategory createCategory(CategoryInfo categoryInfo)
			throws NotSetedMandatoryFieldsException, NotAuthorizedException, 
			BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, 
			NotImplementedException, InternalServerErrorException, NotFoundException, AlreadyExistsException {
		
		checkMandatoryFields(categoryInfo);
		
		String postUrl = _urlBuilder.createCategoryUrl();
		IFlatCategory result = null;
		
		try {
			String responseString = excecutePOSTRequest(postUrl, categoryInfo);
			result = (IFlatCategory) ZendeskContentJSONIntepreters.CATEGORY_INT.getValue().getItem(responseString, _forumDataParser);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	
	
	@Override
	public IFlatCategory updateCategory(int categoryId, CategoryInfo catNewInfo)
			throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, 
			NotAuthorizedException, BadRequestException, UnprocessableEntityException, 
			ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, 
			InvalidRecordException {
		
		IFlatCategory result = null;
		
		catNewInfo.setId(categoryId);
		checkUpdateFields(catNewInfo);
		
		String putUrl = _urlBuilder.updateCategoryUrl(categoryId);
		
		try {
			
			String responseString = excecutePUTRequest(putUrl, catNewInfo);
			result = (IFlatCategory) ZendeskContentJSONIntepreters.CATEGORY_INT.getValue().getItem(responseString, _forumDataParser);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.CATEGORY, categoryId);
		}
		
		return result;
	}

	@Override
	public boolean deleteCategory(int categoryId)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		String deleteUrl = _urlBuilder.deleteCategoryUrl(categoryId);

		try {
			HttpResponse response = excecuteDELETERequest(deleteUrl);
		
			if(response.getStatusLine().getStatusCode() == Constants.SERVICE_API_RESPONSE_OK)
				return true;
			
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.CATEGORY,categoryId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	/*
	 * 
	 * ======================================================
	 * 
	 *				FORA 
	 *
	 *=======================================================
	 *
	 */
	
	
	@Override
	public List<IFlatForum> getAllForums() 
			throws NotAuthorizedException, BadRequestException, 
				UnprocessableEntityException, ServiceUnavaliableException, 
				NotImplementedException, InternalServerErrorException, 
				NotFoundException, InvalidRecordException {
		
		
		String getUrl = _urlBuilder.listForumsUrl();
		List<IFlatForum> result = new LinkedList<IFlatForum>();
		
		String responseString = excecuteGETRequest(getUrl);
		for(ICommon item : ZendeskContentJSONIntepreters.FORUM_INT.getValue().getListOfItems(responseString, _forumDataParser))
			result.add((IFlatForum) item);
		
		return result;
	}

	@Override
	public List<IFlatForum> getCategoryForums(int categoryId)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, 
			BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, 
			NotImplementedException, InternalServerErrorException, InvalidRecordException {
		
		String getUrl = _urlBuilder.listForumsUrl(categoryId);
		List<IFlatForum> result = new LinkedList<IFlatForum>();
		
		try {
			
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.FORUM_INT.getValue().getListOfItems(responseString, _forumDataParser))
				result.add((IFlatForum) item);
			
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.CATEGORY, categoryId);
		}
		
		return result;
		
	}

	@Override
	public IFlatForum getForum(int forumId) 
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, 
			BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, 
			NotImplementedException, InternalServerErrorException, InvalidRecordException {
		
		String getUrl = _urlBuilder.showForumUrl(forumId);
		IFlatForum result = null;
		
		try {
			
			String responseString = excecuteGETRequest(getUrl);
			result = (IFlatForum) ZendeskContentJSONIntepreters.FORUM_INT.getValue().getItem(responseString, _forumDataParser);
			
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.FORUM, forumId);
		}
		
		return result;
	}

	/*
	 *  BUG API - tworzy sie forum dla nie istniejacej kategorii (wiec uważać!!!)
	 */
	
	@Override
	public IFlatForum createForum(ForumInfo forumInfo)
			throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException,
			NotAuthorizedException, BadRequestException, UnprocessableEntityException, 
			ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, 
			NotFoundException, InvalidRecordException {

		checkMandatoryFields(forumInfo);
		
		IFlatForum result = null;
		String postUrl = _urlBuilder.createForumUrl();
		
		try {
	
			String responseString = excecutePOSTRequest(postUrl, forumInfo);
			result = (IFlatForum) ZendeskContentJSONIntepreters.FORUM_INT.getValue().getItem(responseString, _forumDataParser);
	
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public IFlatForum updateForum(int forumId, ForumInfo forumNewInfo)
			throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, 
			NotAuthorizedException, BadRequestException, UnprocessableEntityException,
			ServiceUnavaliableException, NotImplementedException, 
			InternalServerErrorException, InvalidRecordException {
		
		forumNewInfo.setId(forumId);
		checkUpdateFields(forumNewInfo);
		
		String putUrl = _urlBuilder.updateForumUrl(forumId);
		IFlatForum result = null;
		
		try {

			String responseString = excecutePOSTRequest(putUrl, forumNewInfo);
			result = (IFlatForum) ZendeskContentJSONIntepreters.FORUM_INT.getValue().getItem(responseString, _forumDataParser);

		} catch (IOException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.FORUM, forumId);
		}
		return result;
	}

	/*
	 *  BUG API - kiedy tworzymy forum, zmieniamy go (np przenosimy do innej kategorii) a potem kasujemy to on pojawia się w 
	 *  starej kategorii w takim stanie jaki mial przez zmiana
	 * 
	 */
	
	@Override
	public boolean deleteForum(int forumId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, 
			BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, 
			NotImplementedException, InternalServerErrorException {
		
		String deleteUrl = _urlBuilder.deleteForumUrl(forumId);
		try {
			
			HttpResponse response = excecuteDELETERequest(deleteUrl);
			
			if(response.getStatusLine().getStatusCode() == Constants.SERVICE_API_RESPONSE_OK)
				return true;
		
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.FORUM,forumId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return false;
		
	}
	
	/*
	 * 
	 * =========================================================================
	 * 
	 *  			WATKI
	 *  
	 *  ========================================================================
	 *  
	 */
	
	
	@Override
	public List<IFlatTopic> getAllTopics() 
			throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, 
			ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException {
		
		String getUrl = _urlBuilder.listTopicsUrl();
		List<IFlatTopic> result = new LinkedList<IFlatTopic>();
		
		try {
			
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.TOPIC_INT.getValue().getListOfItems(responseString, _forumDataParser))
				result.add((IFlatTopic) item);
		
		} catch (InvalidRecordException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public List<IFlatTopic> getForumTopics(int forumId)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
			UnprocessableEntityException, ServiceUnavaliableException,
			NotImplementedException, InternalServerErrorException {
		
		
		String getUrl = _urlBuilder.listTopicsOfForumUrl(forumId);
		List<IFlatTopic> result = new LinkedList<IFlatTopic>();
		
		try {
			
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.TOPIC_INT.getValue().getListOfItems(responseString, _forumDataParser))
				result.add((IFlatTopic) item);
		
		} catch (InvalidRecordException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.FORUM, forumId);
		}
		
		return result;
		
	}

	@Override
	public List<IFlatTopic> getUserTopics(int userId)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		
		String getUrl = _urlBuilder.listTopicsOfUsetUrl(userId);
		List<IFlatTopic> result = new LinkedList<IFlatTopic>();
		
		try {
			
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.TOPIC_INT.getValue().getListOfItems(responseString, _forumDataParser))
				result.add((IFlatTopic) item);
	
		} catch (InvalidRecordException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.USER, userId);
		}
		
		return result;
		
	}

	@Override
	public IFlatTopic getTopic(int topicId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		String getUrl = _urlBuilder.showTopicUrl(topicId);
		IFlatTopic result = null;
		
		try {
			String responseString = excecuteGETRequest(getUrl);
			result = (IFlatTopic) ZendeskContentJSONIntepreters.TOPIC_INT.getValue().getItem(responseString, _forumDataParser);
		
		} catch (InvalidRecordException e) {
			System.out.println(e.toString());
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.TOPIC, topicId);
		}
		
		return result;
	}

	@Override
	public IFlatTopic createTopic(TopicInfo topicInfo)
			throws NotSetedMandatoryFieldsException, NotAuthorizedException, 
			BadRequestException, UnprocessableEntityException, 
			ServiceUnavaliableException, NotImplementedException, 
			InternalServerErrorException, NotFoundException, InvalidRecordException {
		
		checkMandatoryFields(topicInfo);
		String postUrl = _urlBuilder.createTopicUrl();
		IFlatTopic result = null;
		
		try {
			
			String responseString = excecutePOSTRequest(postUrl, topicInfo);
			result = (IFlatTopic) ZendeskContentJSONIntepreters.TOPIC_INT.getValue().getItem(responseString, _forumDataParser);
		
		} catch (IOException e) {
			e.printStackTrace();
		} 		

		return result;
	}

	@Override
	public IFlatTopic updateTopic(int topicId, TopicInfo topicNewInfo)
			throws RemoteObjectDoesNotExistException, NotSetedMandatoryFieldsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException {
		
		topicNewInfo.setId(topicId);
		checkUpdateFields(topicNewInfo);
		
		String putUrl = _urlBuilder.updateTopicUrl(topicId);
		IFlatTopic result = null;
		
		try {
			String responseString = excecutePUTRequest(putUrl, topicNewInfo);
			result = (IFlatTopic) ZendeskContentJSONIntepreters.TOPIC_INT.getValue().getItem(responseString, _forumDataParser);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.TOPIC, topicId);
		}
		
		return result;
	}

	@Override
	public boolean deleteTopic(int topicId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		String deleteUrl = _urlBuilder.deleteTopicUrl(topicId);
		try {
			HttpResponse response = excecuteDELETERequest(deleteUrl);
			
			if(response.getStatusLine().getStatusCode() == Constants.SERVICE_API_RESPONSE_OK)
				return true;
		
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.TOPIC,topicId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	
	/*
	 * 
	 * ================================================
	 * 
	 * 				KOMENTARZE
	 * 
	 * ================================================
	 * 
	 */
	
	
	@Override
	public List<IFlatComment> getTopicComments(int topicId)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, 
			UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, 
			InternalServerErrorException, InvalidRecordException {
		
		String getUrl = _urlBuilder.listCommentsTopicUrl(topicId);
		List<IFlatComment> result = new LinkedList<IFlatComment>();
		try {
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.COMMENT_INT.getValue().getListOfItems(responseString, _forumDataParser))
				result.add((IFlatComment) item);
			
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.TOPIC, topicId);
		}
		
		return result;
	}

	@Override
	public List<IFlatComment> getUserComments(int userId)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException {
		
		String getUrl = _urlBuilder.listCommentsUserUrl(userId);
		List<IFlatComment> result = new LinkedList<IFlatComment>();
		try {
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.COMMENT_INT.getValue().getListOfItems(responseString, _forumDataParser))
				result.add((IFlatComment) item);
			
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.USER, userId);
		}
		
		return result;
	}

	@Override
	public IFlatComment getTopicComment(int topicId, int commentId)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
			UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException,
			InternalServerErrorException, InvalidRecordException {

		String getUrl = _urlBuilder.showCommentTopicUrl(topicId, commentId);
		IFlatComment result =  null;
		try {
			String responseString = excecuteGETRequest(getUrl);
			result = (IFlatComment) ZendeskContentJSONIntepreters.COMMENT_INT.getValue().getItem(responseString, _forumDataParser);
			
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.TOPIC, topicId);
		}
		
		return result;
	}

	@Override
	public IFlatComment getUserComment(int userId, int commentId)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
			UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException,
			InternalServerErrorException, InvalidRecordException {

		String getUrl = _urlBuilder.showCommentUserUrl(userId, commentId);
		IFlatComment result =  null;
		
		try {
			String responseString = excecuteGETRequest(getUrl);
			result = (IFlatComment) ZendeskContentJSONIntepreters.COMMENT_INT.getValue().getItem(responseString, _forumDataParser);
			
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.USER, userId);
		}
		
		return result;
	}

	@Override
	public IFlatComment createComment(int topicId, CommentInfo commentInfo)
			throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException {
		
		commentInfo.setTopicId(topicId);
//		checkMandatoryFields(commentInfo);
		
		String postUrl = _urlBuilder.createCommentUrl(topicId);
		IFlatComment result =  null;
	
		try {
			String responseString = excecutePOSTRequest(postUrl, commentInfo);
			result = (IFlatComment) ZendeskContentJSONIntepreters.COMMENT_INT.getValue().getItem(responseString, _forumDataParser);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.TOPIC, topicId);
		} 
		
		return result;
		
	}

	@Override
	public IFlatComment updateComment(int topicId, int commentId,
			CommentInfo commentInfo) throws NotSetedMandatoryFieldsException,
			RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, 
			UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException,
			InternalServerErrorException, InvalidRecordException {
		
		
		commentInfo.setTopicId(topicId).setId(commentId);
		checkUpdateFields(commentInfo);
		
		String putUrl = _urlBuilder.updateCommentUrl(topicId, commentId);
		IFlatComment result =  null;
	
		try {
			String responseString = excecutePUTRequest(putUrl, commentInfo);
			result = (IFlatComment) ZendeskContentJSONIntepreters.COMMENT_INT.getValue().getItem(responseString, _forumDataParser);
			
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.TOPIC, topicId);
		} 
		
		return result;
	}

	@Override
	public boolean deleteComment(int topicId, int commentId)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		String deleteUrl = _urlBuilder.deleteCommentUrl(topicId, commentId);
		try {

			HttpResponse response = excecuteDELETERequest(deleteUrl);
			if(response.getStatusLine().getStatusCode() == Constants.SERVICE_API_RESPONSE_OK)
				return true;
		
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.TOPIC,topicId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	
	/*
	 * 
	 * ====================================================
	 * 
	 *		 			UZYTKOWNICY
	 * 
	 * ====================================================
	 * 
	 */
	
	@Override
	public List<IFlatUser> getAllUsers() throws NotAuthorizedException, BadRequestException,
		UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, 
		InternalServerErrorException, NotFoundException, InvalidRecordException {
		
		List<IFlatUser> result = new LinkedList<IFlatUser>();
		String getUrl = _urlBuilder.listUsersUrl();
		String responseString = excecuteGETRequest(getUrl);
		
		for(ICommon item : ZendeskContentJSONIntepreters.USER_INT.getValue().getListOfItems(responseString, _forumDataParser))
			result.add((IFlatUser) item);
		
		return result;
	}

	@Override
	public IFlatUser getUser(int userId) throws RemoteObjectDoesNotExistException, NotAuthorizedException, 
		BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, 
		NotImplementedException, InternalServerErrorException {
		
		IFlatUser result = null;
		String getUrl = _urlBuilder.showUserUrl(userId);
		try {
			String responseString = excecuteGETRequest(getUrl);
			result = (IFlatUser) ZendeskContentJSONIntepreters.USER_INT.getValue().getItem(responseString, _forumDataParser);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.USER, userId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return result;
		
	}

	@Override
	public IFlatUser createUser(UserInfo userInfo)
			throws NotSetedMandatoryFieldsException, NotAuthorizedException, BadRequestException,
			UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, 
			InternalServerErrorException, NotFoundException, InvalidRecordException {
		
		checkMandatoryFields(userInfo);
		IFlatUser result = null;
		String postUrl = _urlBuilder.createUserUrl();
		
		try {
			String responseString = excecutePOSTRequest(postUrl, userInfo);
			result = (IFlatUser) ZendeskContentJSONIntepreters.USER_INT.getValue().getItem(responseString, _forumDataParser);
			
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} 
		
		return result;
	}

	@Override
	public boolean deleteUser(int userId) throws RemoteObjectDoesNotExistException,
		NotAuthorizedException, BadRequestException, UnprocessableEntityException,
		ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		String deleteUrl = _urlBuilder.deleteUserUrl(userId);
		try {

			HttpResponse response = excecuteDELETERequest(deleteUrl);
			if(response.getStatusLine().getStatusCode() == Constants.SERVICE_API_RESPONSE_OK)
				return true;
		
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.USER,userId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	// ===password
	
	private class PropertyInfoInjector extends ContentItemInfo{
		
		private HashMap<String, String > _props = new HashMap<String,String>();

		public PropertyInfoInjector(){
			super(ContentType.USER);
		}
		
		public PropertyInfoInjector pushProps(HashMap<String, String > newProps){
			_props = newProps;
			return this;
		}
		
		@Override
		public String getJSONDescription() {
			List<NameValuePair> values = new LinkedList<NameValuePair>();
			for(String propname : _props.keySet()){
				values.add(new BasicNameValuePair(propname,_props.get(propname)));
			}
			
			return super.getObjectJSONString(values);
		}
		
	}
	
	@Override
	public boolean setUserPassword(int userId, String newPassword)
			throws RemoteObjectDoesNotExistException, UnsupportedEncodingException, 
			NotAuthorizedException, BadRequestException, UnprocessableEntityException,
			ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, 
			InvalidRecordException {
		
		String postUrl = _urlBuilder.setUserPasswordUrl(userId);
		HashMap<String, String> props = new HashMap<String, String>();
		props.put(Constants.USER_PASSWORD_ATTR, newPassword);
		
		HttpResponse response = getResponsePOSTRequest(postUrl, new PropertyInfoInjector().pushProps(props));
	
		try {
			processResponse(response, postUrl);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.USER, userId);
		}
		
		if(response.getStatusLine().getStatusCode() == Constants.SERVICE_API_RESPONSE_OK)
			return true;
	
		return false;
	}



	@Override
	public boolean changeUserPassword(int userId, String oldPassword, String newPassword) throws RemoteObjectDoesNotExistException, UnsupportedEncodingException,
			NotAuthorizedException, BadRequestException, UnprocessableEntityException, 
			ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException {
		
		String putUrl = _urlBuilder.changerUsersPasswordUrl(userId);
		
		HashMap<String,String> props = new HashMap<String, String>();
		props.put(Constants.USER_OLD_PASSWORD_ATTR, oldPassword);
		props.put(Constants.USER_PASSWORD_ATTR, newPassword);
		
		try {
			HttpResponse response = getResponsePUTRequest(putUrl, new PropertyInfoInjector().pushProps(props));
			
			if(response.getStatusLine().getStatusCode() == Constants.SERVICE_API_RESPONSE_OK)
				return true;
			
			processResponse(response, putUrl);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.USER, userId);
		} 
		
		return false;
	}
	
	@Override
	public IFlatUser updateUser(int userId, UserInfo userInfo)
			throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException,
			UnsupportedEncodingException, NotAuthorizedException, BadRequestException, 
			UnprocessableEntityException, ServiceUnavaliableException,
			NotImplementedException, InternalServerErrorException {
		
		userInfo.setId(userId);
		checkUpdateFields(userInfo);
		IFlatUser result = null;
		String putUrl = _urlBuilder.updateUserUrl(userId);
		try {
			String responseString = excecutePUTRequest(putUrl, userInfo);
			result = (IFlatUser) ZendeskContentJSONIntepreters.USER_INT.getValue().getItem(responseString, _forumDataParser);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.USER, userId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Override
	public FlatUserRelatedInfo getUserRelatedInfo(int userId) throws RemoteObjectDoesNotExistException, 
	NotAuthorizedException, BadRequestException, UnprocessableEntityException, 
	ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		FlatUserRelatedInfo result = null;
		
		String getUrl = _urlBuilder.showUserRelatedInfoUrl(userId);
		try {
			String respString = excecuteGETRequest(getUrl);
			result = (FlatUserRelatedInfo) ZendeskContentJSONIntepreters.USER_RELATER_INT.getValue().setProperty(Constants.SERVICE_CONTENT_USER, String.valueOf(userId)).getItem(respString, _forumDataParser);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.USER, userId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		
		return result;
	}
	
	/*
	 *=================================================================
	 *			
	 *					DODATKOWE
	 *
	 * ================================================================
	 */
	
	@Override
	public ICommon reloadWhithAPIUrl(String url, ContentType typeOfContent)
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException {
		
		HttpGet request = new HttpGet(url);
		request = (HttpGet) injectCredentials(request);
		
		ICommon result = null;
		
		try {
		
			HttpResponse response = _requestManager.executeRequest(request);
			String responseString = processResponse(response, url);
			
			JSONObject resultJson = new JSONObject(responseString);

			JSONObject contentJson = resultJson.getJSONObject(typeOfContent.getValue());
			switch(typeOfContent){
			case CATEGORY :{
				result = _forumDataParser.parseCategory(contentJson);
				break;
			}
			case FORUM :{
				result = _forumDataParser.parseForum(contentJson);
				break;
			}
			case TOPIC :{
				result = _forumDataParser.parseTopic(contentJson);
				break;
			}
			case COMMENT :{
				result = _forumDataParser.parseComment(contentJson);
				break;
			}
			case USER :{
				result = _forumDataParser.parseUser(contentJson);
				break;
			}
			case FORUM_SUBSCRIPTION:{
				result = _forumDataParser.parseForumSubscription(contentJson);
				break;
			}
			case TOPIC_SUBSCRIPTION:{
				result = _forumDataParser.parseTopicSubscription(contentJson);
				break;
			}
			case USER_RELATED:{
				int idx_of_users = url.indexOf(Constants.SERVICE_API_USERS) + 1 + Constants.SERVICE_API_USERS.length();
				String user_id_string = url.substring(idx_of_users);
				int idx_of_id_ending = url.indexOf('/');
				int userId = Integer.parseInt(user_id_string.substring(0, idx_of_id_ending + 1));
System.out.println("USER RELATED : url " + url + " taken userId: " + userId); // TODO DELETE it
				result = _forumDataParser.parseUserRelatedInfo(contentJson,userId);
				break;
			}
			}
			
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(typeOfContent, -1);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return result;
	}



	@Override
	public boolean logIn(String user, String password) {
		_userName = user;
		_password = password;
		_isLogedIn = true;
		
		return true;
	}

	@Override
	public void logOut() {
		_isLogedIn = false;
		_password = null;
		_userName = null;
	}


	
	/*
	 *  			SUBSKRYPCJE
	 */
	


	@Override
	public List<IFlatTopicSubscription> getAllTopicSubscriptions() throws NotAuthorizedException, BadRequestException,
			UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, 
			InternalServerErrorException {
		
		String getUrl = _urlBuilder.listTopicSubscriptionsUrl();
		List<IFlatTopicSubscription> result = new LinkedList<IFlatTopicSubscription>();
		
		try {
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.TOPIC_SUBSCRIPTION_INT.getValue().getListOfItems(responseString, _forumDataParser)){
				result.add((IFlatTopicSubscription) item);
			}
			
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		} 
		
		return result;
	}


	@Override
	public List<IFlatTopicSubscription> getTopicSubscriptions(int topicId) throws NotAuthorizedException, 
		BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, 
		NotImplementedException, InternalServerErrorException {
		
		String getUrl = _urlBuilder.listTopicSubscriptionsUrl(topicId);
		List<IFlatTopicSubscription> result = new LinkedList<IFlatTopicSubscription>();
		
		try {
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.TOPIC_SUBSCRIPTION_INT.getValue().getListOfItems(responseString, _forumDataParser))
				result.add((IFlatTopicSubscription) item);
			
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return result;
	}


	@Override
	public IFlatTopicSubscription getTopicSubscription(int subscriptionId) throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		String getUrl = _urlBuilder.showTopicSubscriptionUrl(subscriptionId);
		IFlatTopicSubscription result = null;
		
		try {
		
			String responseString = excecuteGETRequest(getUrl);
			result = (IFlatTopicSubscription) ZendeskContentJSONIntepreters.TOPIC_SUBSCRIPTION_INT.getValue().getItem(responseString, _forumDataParser);
		
		} catch (NotFoundException e) {
			e.printStackTrace();
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return result;
	}

	
	@Override
	public IFlatTopicSubscription createTopicSubscription(int topicId,
			int userId) throws UnsupportedEncodingException, NotAuthorizedException,
			BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
			NotImplementedException, InternalServerErrorException, NotFoundException,
			InvalidRecordException {
		
		String postUrl = _urlBuilder.createTopicSubscriptionUrl();
		String responseString = excecutePOSTRequest(postUrl, new ContentItemInfo(ContentType.TOPIC_SUBSCRIPTION) {
			
			private int _topicId;
			private int _userId;

			public ContentItemInfo initialize(int topicId, int userId){
				_topicId = topicId;
				_userId = userId;
				
				return this;
			}
			
			@Override
			public String getJSONDescription() {
				List<NameValuePair> values = new LinkedList<NameValuePair>();
				
				values.add(new BasicNameValuePair(Constants.SUBSCRIPTION_USER_ID_ATTR, String.valueOf(_userId)));
				values.add(new BasicNameValuePair(Constants.SUBSCRIPTION_TOPIC_TOPIC_ID_ATTR,String.valueOf(_topicId)));
				
				return super.getObjectJSONString(values);
			}
			
		}.initialize(topicId, userId));
	
		IFlatTopicSubscription result = (IFlatTopicSubscription) ZendeskContentJSONIntepreters.TOPIC_SUBSCRIPTION_INT.getValue().getItem(responseString, _forumDataParser);
		
		return result;
	}
	
	@Override
	public boolean deleteTopicSubscription(int subscriptionId) throws NotAuthorizedException, BadRequestException, 
			UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, 
			InternalServerErrorException, RemoteObjectDoesNotExistException {
		
		String deleteUrl = _urlBuilder.deleteTopicSubscriptionUrl(subscriptionId);
		try {
		
			HttpResponse response = excecuteDELETERequest(deleteUrl);
			if(response.getStatusLine().getStatusCode() == Constants.SERVICE_API_RESPONSE_OK)
				return true;
		
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.TOPIC_SUBSCRIPTION, subscriptionId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return false;
	}

	
	@Override
	public List<IFlatForumSubscription> getAllForumSubscriptions() throws NotAuthorizedException,
			BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
			NotImplementedException, InternalServerErrorException{
		
		String getUrl = _urlBuilder.listForumSubscriptionsUrl();
		List<IFlatForumSubscription> result = new LinkedList<IFlatForumSubscription>();
		
		try {
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.FORUM_SUBSCRIPTION_INT.getValue().getListOfItems(responseString, _forumDataParser))
				result.add((IFlatForumSubscription) item);
		
		}catch( NotFoundException e){ 
			e.printStackTrace();
		}catch (InvalidRecordException e) {
			e.printStackTrace();
		}
	
		return result;
	}


	@Override
	public List<IFlatForumSubscription> getForumSubscriptions(int forumId) throws NotAuthorizedException, BadRequestException, 
	UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, 
	InternalServerErrorException {
		
		String getUrl = _urlBuilder.listForumSubscriptionsUrl(forumId);
		List<IFlatForumSubscription> result = new LinkedList<IFlatForumSubscription>();
		
		try {
			String responseString = excecuteGETRequest(getUrl);
			for(ICommon item : ZendeskContentJSONIntepreters.FORUM_SUBSCRIPTION_INT.getValue().getListOfItems(responseString, _forumDataParser))
				result.add((IFlatForumSubscription) item);
		
		}catch( NotFoundException e){ 
			e.printStackTrace();
		}catch (InvalidRecordException e) {
			e.printStackTrace();
		}
	
		return result;
	}


	@Override
	public IFlatForumSubscription getForumSubscription(int subscriptionId) throws RemoteObjectDoesNotExistException,
			NotAuthorizedException, BadRequestException, UnprocessableEntityException, 
			ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		IFlatForumSubscription result = null;
		String getUrl = _urlBuilder.showForumSubscriptionUrl(subscriptionId);
		try {
			String responseString = excecuteGETRequest(getUrl);
			result = (IFlatForumSubscription) ZendeskContentJSONIntepreters.FORUM_SUBSCRIPTION_INT.getValue().getItem(responseString, _forumDataParser);
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.FORUM_SUBSCRIPTION, subscriptionId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return result;
	}


	@Override
	public boolean deleteForumSubscription(int subscriptionId) throws RemoteObjectDoesNotExistException,
			NotAuthorizedException, BadRequestException, UnprocessableEntityException, 
			ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		String deleteUrl = _urlBuilder.deleteForumSubscriptionUrl(subscriptionId);
		
		try {
		
			HttpResponse response = excecuteDELETERequest(deleteUrl);
			if(response.getStatusLine().getStatusCode() == Constants.SERVICE_API_RESPONSE_OK)
				return true;
		
		} catch (NotFoundException e) {
			e.printStackTrace();
			throw new RemoteObjectDoesNotExistException(ContentType.FORUM_SUBSCRIPTION, subscriptionId);
		} catch (InvalidRecordException e) {
			e.printStackTrace();
		}
		
		return false;
		
	}

	@Override
	public IFlatForumSubscription createForumSubscription(int forumId,
			int userId) throws UnsupportedEncodingException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException {
		
		String postUrl = _urlBuilder.createForumSubscriptionUrl();
		String responseString = excecutePOSTRequest(postUrl, new ContentItemInfo(ContentType.FORUM_SUBSCRIPTION) {
			
			private int _forumId;
			private int _userId;

			public ContentItemInfo initialize(int forumId, int userId){
				_forumId = forumId;
				_userId = userId;
				
				return this;
			}
			
			@Override
			public String getJSONDescription() {
				List<NameValuePair> values = new LinkedList<NameValuePair>();
				
				values.add(new BasicNameValuePair(Constants.SUBSCRIPTION_USER_ID_ATTR, String.valueOf(_userId)));
				values.add(new BasicNameValuePair(Constants.SUBSCRIPTION_FORUM_FORUM_ID_ATTR,String.valueOf(_forumId)));
				
				return super.getObjectJSONString(values);
			}
			
		}.initialize(forumId, userId));
	
		IFlatForumSubscription result = (IFlatForumSubscription) ZendeskContentJSONIntepreters.FORUM_SUBSCRIPTION_INT.getValue().getItem(responseString, _forumDataParser);
		
		return result;
		
	}



	@Override
	public IAPIUrlBuilder getAPIUrlBuilder() {
		return _urlBuilder;
	}


}
