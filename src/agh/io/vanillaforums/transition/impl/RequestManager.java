package agh.io.vanillaforums.transition.impl;


import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;

import agh.io.vanillaforums.transition.interfaces.managing.IRequestManager;

public class RequestManager implements IRequestManager {

	private HttpClient _httpClient;
	
	public RequestManager(){
		_httpClient = new DefaultHttpClient();
	}

	@Override
	public HttpResponse executeRequest(HttpRequestBase request) throws ClientProtocolException, IOException {
		return _httpClient.execute(request);
	}

}
