package agh.io.vanillaforums.transition.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.items.IFlatCategory;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.AlreadyExistsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.CategoryInfo;
import agh.io.vanillaforums.transition.iteminfo.ForumInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.utils.Constants;
import agh.io.vanillaforums.transition.utils.IContentJSONDataInterpreter;

public class FlatCategory extends AbstractFlatContent implements IFlatCategory
{
    private String _description;
    private String _name;
    private int _position;
    private boolean _hasChanged;

    public FlatCategory( String name, String description, int id, int position, String apiUrl, Date creationDate, Date updateDate,
            IContentRemoteManager man )
    {
        super( ContentType.CATEGORY, id, creationDate, updateDate, apiUrl, man );
        _name = name;
        _description = description;
        _position = position;
    }

    @Override
    public CategoryInfo getInfo()
    {
        CategoryInfo result = new CategoryInfo();
        result.setCategoryDescription( _description ).setCategoryName( _name ).setPosition( _position ).setId( _id );
        return result;
    }

    /*
     * Metody lokalne
     */
    @Override
    public String getName()
    {
        return _name;
    }

    @Override
    public String getDescription()
    {
        return _description;
    }

    @Override
    public int getPosition()
    {
        return _position;
    }

    @Override
    protected void reloadContent( ICommon whatDownload )
    {
        IFlatCategory other = (IFlatCategory)whatDownload;
        _name = other.getName();
        _description = other.getDescription();
        _creationDate = other.getCreationDate();
        _updateDate = other.getUpdateDate();
        _position = other.getPosition();
    }

    /*
     * Metody zdalne
     */
    @Override
    public List<IFlatForum> getForums() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            NotFoundException, InvalidRecordException
    {
        return _remoteContentManager.getCategoryForums( _id );
    }

    @Override
    public IFlatForum getForum( int forumId ) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            NotFoundException, InvalidRecordException
    {
        return _remoteContentManager.getForum( forumId );
    }

    @Override
    public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException
    {
        return _remoteContentManager.deleteCategory( _id );
    }

    @Override
    public boolean update( CategoryInfo newInfo ) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException,
            NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
            NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException
    {
        IFlatCategory result = _remoteContentManager.updateCategory( _id, newInfo );
        if( result == null )
        {
            return false;
        }
        reloadContent( result );
        return true;
    }

    @Override
    public IFlatForum createForum( ForumInfo forumInfo ) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException,
            AlreadyExistsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
            NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException
    {
        forumInfo.setParentCategoryId( _id );
        return _remoteContentManager.createForum( forumInfo );
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append( "FlatCategory : \n" );
        sb.append( "\tid : " );
        sb.append( _id );
        sb.append( "\n\turl : " );
        sb.append( _apiUrl );
        sb.append( "\n\tname : " );
        sb.append( _name );
        sb.append( "\n\tdescription : " );
        sb.append( _description );
        sb.append( "\n\tposition : " );
        sb.append( _position );
        sb.append( "\n\tcreated at : " );
        sb.append( Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString( _creationDate ) );
        sb.append( "\n\tupdated at : " );
        sb.append( Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString( _updateDate ) );
        sb.append( "\n\n" );
        return sb.toString();
    }

    private static IContentJSONDataInterpreter __interpreterInstanse = null;

    public static IContentJSONDataInterpreter getInterpreter()
    {
        if( __interpreterInstanse == null )
        {
            __interpreterInstanse = new IContentJSONDataInterpreter()
            {
                private HashMap<String, String> _propMap = new HashMap<String, String>();

                @SuppressWarnings( "finally" )
                @Override
                public ICommon getItem( String source, IJSONForumDataParser parser )
                {
                    ICommon result = null;
                    try
                    {
                        JSONObject categoryJson = new JSONObject( source );
                        JSONObject categoryObjJson = categoryJson.getJSONObject( Constants.SERVICE_API_CATEGORY );
                        result = parser.parseCategory( categoryObjJson );
                    }
                    catch( JSONException e )
                    {
                        e.printStackTrace();
                    }
                    finally
                    {
                        return result;
                    }
                }

                @SuppressWarnings( "finally" )
                @Override
                public List<ICommon> getListOfItems( String source, IJSONForumDataParser parser )
                {
                    List<ICommon> resultList = new LinkedList<ICommon>();
                    try
                    {
                        JSONObject jsonResult = new JSONObject( source );
                        JSONArray categoriesJson = jsonResult.getJSONArray( Constants.SERVICE_API_CATEGORIES );
                        for( int i = 0; i < categoriesJson.length(); ++i )
                        {
                            JSONObject categoryJson = categoriesJson.getJSONObject( i );
                            IFlatCategory flatCategory = parser.parseCategory( categoryJson );
                            resultList.add( flatCategory );
                        }
                    }
                    catch( JSONException e )
                    {
                        e.printStackTrace();
                    }
                    finally
                    {
                        return resultList;
                    }
                }

                @Override
                public IContentJSONDataInterpreter setProperty( String name, String value )
                {
                    _propMap.put( name, value );
                    return this;
                }
            };
        }
        return __interpreterInstanse;
    }

    @Override
    public boolean hasChanged()
    {
        return _hasChanged;
    }

    @Override
    public void setHasChanged( boolean hasChanged )
    {
        _hasChanged = hasChanged;
    }
}
