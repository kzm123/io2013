package agh.io.vanillaforums.transition.impl;

import java.util.Date;

import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;

public abstract class AbstractFlatContent implements ICommon {

	protected int _id;
	protected Date _updateDate;
	protected Date _creationDate;
	protected String _apiUrl;
	protected IContentRemoteManager _remoteContentManager;
	protected ContentType _typeOfContent;
	
	public AbstractFlatContent(ContentType type, int id, Date creationDate, Date updateDate, String apiUrl, IContentRemoteManager remoteManger) {
		_typeOfContent = type;
		_id = id;
		_creationDate = creationDate;
		_updateDate = updateDate;
		_apiUrl = apiUrl;
		_remoteContentManager = remoteManger;
	}

	@Override
	public Date getCreationDate() {
		return _creationDate;
	}

	@Override
	public Date getUpdateDate() {
		return _updateDate;
	}

	@Override
	public int getId() {
		return _id;
	}

	
	protected abstract void reloadContent(ICommon whatDownload);
	
	public boolean reloadIfExists() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException{
		try {
			
			ICommon updated = _remoteContentManager.reloadWhithAPIUrl(_apiUrl, _typeOfContent);
			reloadContent(updated);
			
		} catch (RemoteObjectDoesNotExistException e) {
			System.out.println(e.toString());
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

}