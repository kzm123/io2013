package agh.io.vanillaforums.transition.impl;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.items.IFlatCategory;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForumSubscription;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.AlreadyExistsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.ForumInfo;
import agh.io.vanillaforums.transition.iteminfo.TopicInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.ForumAccessType;
import agh.io.vanillaforums.transition.iteminfo.utils.ForumType;
import agh.io.vanillaforums.transition.utils.Constants;
import agh.io.vanillaforums.transition.utils.IContentJSONDataInterpreter;

public class FlatForum extends AbstractFlatContent implements IFlatForum
{
    private int _parentCategoryId;
    private String _forumName;
    private String _forumDescription;
    private int _position;
    private ForumAccessType _accessType;
    private ForumType _type;
    private boolean _isLocked;
    private boolean _hasChanged;

    public FlatForum( int id, String name, String description, int parentCategoryId, boolean locked, int position,
            ForumAccessType accessType, ForumType type, Date creationDate, Date updateDate, String apiUrl,
            IContentRemoteManager remoteManger )
    {
        super( ContentType.FORUM, id, creationDate, updateDate, apiUrl, remoteManger );
        _parentCategoryId = parentCategoryId;
        _forumName = name;
        _forumDescription = description;
        _position = position;
        _accessType = accessType;
        _type = type;
        _isLocked = locked;
    }

    @Override
    public ForumInfo getInfo()
    {
        ForumInfo result = new ForumInfo();
        result.setParentCategoryId( _parentCategoryId ).setForumName( _forumName ).setForumDescription( _forumDescription )
                .setForumPosition( _position ).setForumAccessType( _accessType ).setForumType( _type ).setIsLocked( _isLocked ).setId( _id );
        return result;
    }

    /*
     * Metody zdalne
     */
    @Override
    public List<IFlatTopic> getTopics() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException
    {
        return _remoteContentManager.getForumTopics( _id );
    }

    @Override
    public boolean update( ForumInfo forumNewInfo ) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException,
            NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
            NotImplementedException, InternalServerErrorException, InvalidRecordException
    {
        IFlatForum newForum = _remoteContentManager.updateForum( _id, forumNewInfo );
        if( newForum == null )
        {
            return false;
        }
        reloadContent( newForum );
        return true;
    }

    @Override
    public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException
    {
        return _remoteContentManager.deleteForum( _id );
    }

    @Override
    public IFlatCategory parentCategory() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException
    {
        return _remoteContentManager.getCategory( _parentCategoryId );
    }

    @Override
    public IFlatTopic createTopic( TopicInfo topicInfo ) throws NotSetedMandatoryFieldsException, NotAuthorizedException,
            BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException,
            InternalServerErrorException, NotFoundException, AlreadyExistsException, InvalidRecordException
    {
        topicInfo.setParentForumId( _id );
        return _remoteContentManager.createTopic( topicInfo );
    }

    private boolean lock( boolean value ) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException
    {
        if( _isLocked != value )
        {
            ForumInfo inf = new ForumInfo();
            inf.setIsLocked( value ).setParentCategoryId( _parentCategoryId ).setId( _id );
            try
            {
                _remoteContentManager.updateForum( _id, inf );
            }
            catch( NotSetedMandatoryFieldsException e )
            {
                e.printStackTrace();
                return false;
            }
            _isLocked = value;
        }
        return true;
    }

    @Override
    public boolean lockForum() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException
    {
        return lock( true );
    }

    @Override
    public boolean unlockForum() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException
    {
        return lock( false );
    }

    /*
     * Metody lokalne
     */
    @Override
    public int getParentCategoryId()
    {
        return _parentCategoryId;
    }

    @Override
    public String getName()
    {
        return _forumName;
    }

    @Override
    public String getDescription()
    {
        return _forumDescription;
    }

    @Override
    public int getPosition()
    {
        return _position;
    }

    @Override
    public ForumAccessType getAccessType()
    {
        return _accessType;
    }

    @Override
    public ForumType getType()
    {
        return _type;
    }

    @Override
    public boolean isLocked()
    {
        return _isLocked;
    }

    @Override
    protected void reloadContent( ICommon whatDownload )
    {
        IFlatForum other = (IFlatForum)whatDownload;
        _forumName = other.getName();
        _forumDescription = other.getDescription();
        _position = other.getPosition();
        _updateDate = other.getUpdateDate();
        _creationDate = other.getCreationDate();
        _type = other.getType();
        _accessType = other.getAccessType();
        _parentCategoryId = other.getParentCategoryId();
        _isLocked = other.isLocked();
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer();
        sb.append( "FlatForum ::\n" );
        sb.append( "\tid : " );
        sb.append( _id );
        sb.append( "\n\tname : " );
        sb.append( _forumName );
        sb.append( "\n\tdescription : " );
        sb.append( _forumDescription );
        sb.append( "\n\tparent_category : " );
        sb.append( _parentCategoryId );
        sb.append( "\n\tisLocked : " );
        sb.append( _isLocked );
        sb.append( "\n\tposition : " );
        sb.append( _position );
        sb.append( "\n\ttype : " );
        sb.append( _type.getValue() );
        sb.append( "\n\taccess : " );
        sb.append( _accessType.getValue() );
        sb.append( "\n\tcreated at : " );
        sb.append( Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString( _creationDate ) );
        sb.append( "\n\tupdated at : " );
        sb.append( Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString( _updateDate ) );
        sb.append( "\n\n" );
        return sb.toString();
    }

    @Override
    public IFlatForumSubscription subscribeFor( IFlatUser subscriber ) throws UnsupportedEncodingException, NotAuthorizedException,
            BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException,
            InternalServerErrorException, NotFoundException, InvalidRecordException
    {
        return _remoteContentManager.createForumSubscription( _id, subscriber.getId() );
    }

    private static IContentJSONDataInterpreter __interpreterInstanse = null;

    public static IContentJSONDataInterpreter getInterpreter()
    {
        if( __interpreterInstanse == null )
        {
            __interpreterInstanse = new IContentJSONDataInterpreter()
            {
                private HashMap<String, String> _propMap = new HashMap<String, String>();

                @SuppressWarnings( "finally" )
                @Override
                public ICommon getItem( String source, IJSONForumDataParser parser )
                {
                    ICommon result = null;
                    try
                    {
                        JSONObject responseJson = new JSONObject( source );
                        JSONObject forumJson = responseJson.getJSONObject( Constants.SERVICE_CONTENT_FORUM );
                        result = parser.parseForum( forumJson );
                    }
                    catch( JSONException e )
                    {
                        e.printStackTrace();
                    }
                    finally
                    {
                        return result;
                    }
                }

                @SuppressWarnings( "finally" )
                @Override
                public List<ICommon> getListOfItems( String source, IJSONForumDataParser parser )
                {
                    List<ICommon> result = new LinkedList<ICommon>();
                    try
                    {
                        JSONObject resultJson = new JSONObject( source );
                        JSONArray forumsJson = resultJson.getJSONArray( Constants.SERVICE_API_FORUMS );
                        for( int i = 0; i < forumsJson.length(); ++i )
                        {
                            JSONObject forumJson = forumsJson.getJSONObject( i );
                            IFlatForum flatForum = parser.parseForum( forumJson );
                            if( flatForum != null )
                                result.add( flatForum );
                        }
                    }
                    catch( JSONException e )
                    {
                        e.printStackTrace();
                    }
                    finally
                    {
                        return result;
                    }
                }

                @Override
                public IContentJSONDataInterpreter setProperty( String name, String value )
                {
                    _propMap.put( name, value );
                    return this;
                }
            };
        }
        return __interpreterInstanse;
    }

    @Override
    public boolean hasChanged()
    {
        return _hasChanged;
    }

    @Override
    public void setHasChanged( boolean hasChanged )
    {
        _hasChanged = hasChanged;
    }
}
