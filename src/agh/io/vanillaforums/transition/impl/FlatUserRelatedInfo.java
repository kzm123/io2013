package agh.io.vanillaforums.transition.impl;

import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.utils.Constants;
import agh.io.vanillaforums.transition.utils.IContentJSONDataInterpreter;

public class FlatUserRelatedInfo extends AbstractFlatContent{

	private int _subscriptionsCount;
	private int _voicesCount;
	private int _topicsCount;
	private int _topicCommentsCount;

	public FlatUserRelatedInfo(int subscriptionsCount, int topicsCount, int topicCommentCount,
			int voicesCount,  String url , IContentRemoteManager remoteManger) {
		super(ContentType.USER_RELATED, -1, null, null,url , remoteManger);
		
		_subscriptionsCount = subscriptionsCount;
		_voicesCount = voicesCount;
		_topicsCount = topicsCount;
		_topicCommentsCount = topicCommentCount;
	}

	@Override
	protected void reloadContent(ICommon whatDownload) {
		FlatUserRelatedInfo other = (FlatUserRelatedInfo) whatDownload;
		
		_subscriptionsCount = other.getCountOfSubscriptions();
		_voicesCount = other.getCountOfVoices();
		_topicsCount = other.getCountOfVoices();
		_topicCommentsCount = other.getCountOfTopicComments();
	}
	
	public int getCountOfTopicComments(){
		return _topicCommentsCount;
	}
	
	public int getCountOfTopics(){
		return _topicsCount;
	}
	
	public int getCountOfVoices(){
		return _voicesCount;
	}
	
	public int getCountOfSubscriptions(){
		return _subscriptionsCount;
	}
	
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
		
		sb	.append("UserRelatedInfo ::")
			.append("\n\tsubscriptionsCount : ")
			.append(_subscriptionsCount)
			.append("\n\tvoicesCount : ")
			.append(_voicesCount)
			.append("\n\ttopicsCount : ")
			.append(_topicsCount)
			.append("\n\ttopicCommentsCount : ")
			.append(_topicCommentsCount)
			.append("\n\n");
		
		return sb.toString();
	}
	
	private static IContentJSONDataInterpreter __interpreterInstanse = null;
	public static IContentJSONDataInterpreter getInterpreter(){
		if(__interpreterInstanse == null){
			__interpreterInstanse = new IContentJSONDataInterpreter() {

				private HashMap<String,String> _propMap = new HashMap<String, String>();
				
				@SuppressWarnings("finally")
				@Override
				public ICommon getItem(String source, IJSONForumDataParser parser) {
					ICommon result = null;
					try{
						JSONObject responseJson = new JSONObject(source);
						JSONObject relatedJson = responseJson.getJSONObject(Constants.SERVICE_CONTENT_USER_RELATED);
						result = parser.parseUserRelatedInfo(relatedJson, Integer.parseInt(_propMap.get(Constants.SERVICE_CONTENT_USER)));
					} catch (JSONException e) {
						e.printStackTrace();
					}finally{
						return result;
					}
				}
				
				@Override
				public List<ICommon> getListOfItems(String source,
						IJSONForumDataParser parser) {
					return null;
				}

				@Override
				public IContentJSONDataInterpreter setProperty(String name,	String value) {
					_propMap.put(name, value);
					return this;
				}
							
				
			};
		}
		return __interpreterInstanse;
	}
}
