package agh.io.vanillaforums.transition.impl;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.items.IFlatComment;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopicSubscription;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.AlreadyExistsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.CommentInfo;
import agh.io.vanillaforums.transition.iteminfo.TopicInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.ForumType;
import agh.io.vanillaforums.transition.utils.Constants;
import agh.io.vanillaforums.transition.utils.IContentJSONDataInterpreter;

public class FlatTopic extends AbstractFlatContent implements IFlatTopic
{
    private String _title;
    private String _body;
    private int _submitterId;
    private int _updatorId;
    private int _forumId;
    private int _commentsCount;
    private int _position;
    private boolean _isLocked;
    private ForumType _topicType;
    private boolean _hasChanged;

    public FlatTopic( int id, String title, String body, ForumType topicType, int submitterId, int updatorId, int parentForum,
            int commentsCount, int position, boolean locked, Date creationDate, Date updateDate, String apiUrl,
            IContentRemoteManager remoteManger )
    {
        super( ContentType.TOPIC, id, creationDate, updateDate, apiUrl, remoteManger );
        _title = title;
        _body = body;
        _submitterId = submitterId;
        _updatorId = updatorId;
        _forumId = parentForum;
        _commentsCount = commentsCount;
        _position = position;
        _isLocked = locked;
        _topicType = topicType;
    }

    @Override
    public TopicInfo getInfo()
    {
        TopicInfo result = new TopicInfo();
        result.setTopicTitle( _title ).setTopicBody( _body ).setParentForumId( _forumId ).setPosition( _position ).setIsLocked( _isLocked )
                .setId( _id );
        return result;
    }

    @Override
    public List<IFlatComment> getComments() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException
    {
        return _remoteContentManager.getTopicComments( _id );
    }

    @Override
    public IFlatForum getParentForum() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException
    {
        return _remoteContentManager.getForum( _forumId );
    }

    @Override
    public IFlatUser getCreator() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException
    {
        return _remoteContentManager.getUser( _submitterId );
    }

    @Override
    public IFlatUser getLastUpdator() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException
    {
        return _remoteContentManager.getUser( _updatorId );
    }

    @Override
    public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException
    {
        return _remoteContentManager.deleteTopic( _id );
    }

    @Override
    public boolean update( TopicInfo topicInfo ) throws RemoteObjectDoesNotExistException, NotSetedMandatoryFieldsException,
            NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
            NotImplementedException, InternalServerErrorException, InvalidRecordException
    {
        IFlatTopic newTopic = _remoteContentManager.updateTopic( _id, topicInfo );
        if( newTopic == null )
        {
            return false;
        }
        reloadContent( newTopic );
        return true;
    }

    @Override
    public IFlatComment addComment( CommentInfo commentInfo ) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException,
            AlreadyExistsException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
            NotImplementedException, InternalServerErrorException, InvalidRecordException
    {
        return _remoteContentManager.createComment( _id, commentInfo );
    }

    private boolean lock( boolean value ) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException
    {
        if( _isLocked != value )
        {
            TopicInfo inf = new TopicInfo();
            inf.setIsLocked( value ).setParentForumId( _forumId ).setId( _id );
            try
            {
                _remoteContentManager.updateTopic( _id, inf );
            }
            catch( NotSetedMandatoryFieldsException e )
            {
                e.printStackTrace();
                return false;
            }
            _isLocked = value;
        }
        return true;
    }

    @Override
    public boolean lockTopic() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException
    {
        return lock( true );
    }

    @Override
    public boolean unlockTopic() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException,
            UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException,
            InvalidRecordException
    {
        return lock( false );
    }

    /*
     * Metody lokalne
     */
    @Override
    protected void reloadContent( ICommon whatDownload )
    {
        IFlatTopic other = (IFlatTopic)whatDownload;
        _title = other.getTitle();
        _body = other.getBody();
        _submitterId = other.getSubmitterId();
        _updatorId = other.getLastUpdatorId();
        _forumId = getParentForumId();
        _commentsCount = other.getCommentsCount();
        _position = other.getPosition();
        _isLocked = other.isLocked();
        _updateDate = other.getUpdateDate();
        _creationDate = other.getCreationDate();
    }

    @Override
    public String getTitle()
    {
        return _title;
    }

    @Override
    public String getBody()
    {
        return _body;
    }

    @Override
    public int getSubmitterId()
    {
        return _submitterId;
    }

    @Override
    public int getLastUpdatorId()
    {
        return _updatorId;
    }

    @Override
    public int getParentForumId()
    {
        return _forumId;
    }

    @Override
    public int getCommentsCount()
    {
        return _commentsCount;
    }

    @Override
    public boolean isLocked()
    {
        return _isLocked;
    }

    @Override
    public int getPosition()
    {
        return _position;
    }

    @Override
    public String toString()
    {
        StringBuffer sb = new StringBuffer( "FlatTopic ::\n" );
        sb.append( "\tid : " );
        sb.append( _id );
        sb.append( "\n\ttitle : " );
        sb.append( _title );
        sb.append( "\n\tbody : " );
        sb.append( _body );
        sb.append( "\n\tparent_forum : " );
        sb.append( _forumId );
        sb.append( "\n\tisLocked : " );
        sb.append( _isLocked );
        sb.append( "\n\tposition : " );
        sb.append( _position );
        sb.append( "\n\ttype : " );
        sb.append( _topicType.getValue() );
        sb.append( "\n\tcomments_count : " );
        sb.append( _commentsCount );
        sb.append( "\n\tcreator_id : " );
        sb.append( _submitterId );
        sb.append( "\n\tupdator_id : " );
        sb.append( _updatorId );
        sb.append( "\n\tcreated at : " );
        sb.append( Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString( _creationDate ) );
        sb.append( "\n\tupdated at : " );
        sb.append( Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString( _updateDate ) );
        sb.append( "\n\n" );
        return sb.toString();
    }

    @Override
    public ForumType getTopicType()
    {
        return _topicType;
    }

    @Override
    public IFlatTopicSubscription subscribeFor( IFlatUser subscriber ) throws UnsupportedEncodingException, NotAuthorizedException,
            BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException,
            InternalServerErrorException, NotFoundException, InvalidRecordException
    {
        return _remoteContentManager.createTopicSubscription( _id, subscriber.getId() );
    }

    private static IContentJSONDataInterpreter __interpreterInstanse = null;

    public static IContentJSONDataInterpreter getInterpreter()
    {
        if( __interpreterInstanse == null )
        {
            __interpreterInstanse = new IContentJSONDataInterpreter()
            {
                private HashMap<String, String> _propMap = new HashMap<String, String>();

                @SuppressWarnings( "finally" )
                @Override
                public ICommon getItem( String source, IJSONForumDataParser parser )
                {
                    ICommon result = null;
                    try
                    {
                        JSONObject jsonResult = new JSONObject( source );
                        JSONObject topicJson = jsonResult.getJSONObject( Constants.SERVICE_CONTENT_TOPIC );
                        result = parser.parseTopic( topicJson );
                    }
                    catch( JSONException e )
                    {
                        e.printStackTrace();
                    }
                    finally
                    {
                        return result;
                    }
                }

                @SuppressWarnings( "finally" )
                @Override
                public List<ICommon> getListOfItems( String source, IJSONForumDataParser parser )
                {
                    List<ICommon> result = new LinkedList<ICommon>();
                    try
                    {
                        JSONObject jsonResult = new JSONObject( source );
                        JSONArray topicsJson = jsonResult.getJSONArray( Constants.SERVICE_API_TOPICS );
                        for( int i = 0; i < topicsJson.length(); ++i )
                        {
                            JSONObject topicJson = topicsJson.getJSONObject( i );
                            IFlatTopic flatTopic = parser.parseTopic( topicJson );
                            result.add( flatTopic );
                        }
                    }
                    catch( JSONException e )
                    {
                        e.printStackTrace();
                    }
                    finally
                    {
                        return result;
                    }
                }

                @Override
                public IContentJSONDataInterpreter setProperty( String name, String value )
                {
                    _propMap.put( name, value );
                    return this;
                }
            };
        }
        return __interpreterInstanse;
    }

    @Override
    public boolean hasChanged()
    {
        return _hasChanged;
    }

    @Override
    public void setHasChanged( boolean hasChanged )
    {
        _hasChanged = hasChanged;
    }
}
