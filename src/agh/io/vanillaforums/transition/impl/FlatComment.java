package agh.io.vanillaforums.transition.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.items.IFlatComment;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.CommentInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.utils.Constants;
import agh.io.vanillaforums.transition.utils.IContentJSONDataInterpreter;

public class FlatComment extends AbstractFlatContent implements IFlatComment {

	private String _body;
	private int _userId;
	private int _topicId;

	public FlatComment(int id, String body, int userId, int topicId, Date creationDate,
			Date updateDate, String apiUrl, IContentRemoteManager remoteManger) {
		super(ContentType.COMMENT, id, creationDate, updateDate, apiUrl, remoteManger);

		_body = body;
		_userId = userId;
		_topicId = topicId;
	
	}

	@Override
	public CommentInfo getInfo() {
		CommentInfo result = new CommentInfo();
		
		result.setCommentBody(_body)
				.setTopicId(_topicId)
				.setCreatorId(_userId)
				.setId(_id);
		
		return result;
	}
	
	/*
	 * 
	 * Metody zdalne
	 * 
	 */
	
	@Override
	public IFlatTopic getParentTopic() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		return _remoteContentManager.getTopic(_topicId);
	}

	@Override
	public IFlatUser getCreator() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		return _remoteContentManager.getUser(_userId);
	}

	@Override
	public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		return _remoteContentManager.deleteComment(_topicId, _id);
	}

	@Override
	public boolean update(CommentInfo commentInfo) throws NotSetedMandatoryFieldsException, RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException {
		IFlatComment newComment = _remoteContentManager.updateComment(_topicId, _id, commentInfo);
		
		if(newComment == null){
			return false;
		}
		
		reloadContent(newComment);
		
		return true;
	}

	/*
	 * 
	 * Metody lokalne
	 * 
	 */
	
	@Override
	public String getBody() {
		return _body;
	}

	@Override
	public int getCreatorId() {
		return _userId;
	}

	@Override
	public int getParentTopicId() {
		return _topicId;
	}

	@Override
	protected void reloadContent(ICommon whatDownload) {
		IFlatComment other = (IFlatComment) whatDownload;
		
		_creationDate = other.getCreationDate();
		_updateDate = other.getUpdateDate();
		_body = other.getBody();
		_topicId = other.getParentTopicId();
		_userId = other.getCreatorId();
		
	}

	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer("FlatComment ::\n");
		
		sb.append("\tid : ");
		sb.append(_id);
		sb.append("\n\tbody : ");
		sb.append(_body);
		sb.append("\n\tparent_topic : ");
		sb.append(_topicId);
		sb.append("\n\tcreator_id : ");
		sb.append(_userId);
		sb.append("\n\tcreated at : ");
		sb.append(Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString(_creationDate));
		sb.append("\n\tupdated at : ");
		sb.append(Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString(_updateDate));
		sb.append("\n\n");
		
		return sb.toString();
	}

	private static IContentJSONDataInterpreter __interpreterInstanse = null;
	public static IContentJSONDataInterpreter getInterpreter(){
		if(__interpreterInstanse == null){
			__interpreterInstanse = new IContentJSONDataInterpreter() {

				private HashMap<String,String> _propMap = new HashMap<String,String>();
				
				@SuppressWarnings("finally")
				@Override
				public ICommon getItem(String source,IJSONForumDataParser parser) {
					ICommon result = null;
					try{
						JSONObject responseJson = new JSONObject(source);
						JSONObject commentJson = responseJson.getJSONObject(Constants.SERVICE_CONTENT_COMMENT);
						result = parser.parseComment(commentJson);
					}catch(JSONException e){
						e.printStackTrace();
					}finally{
						return result;
					}
				}

				@SuppressWarnings("finally")
				@Override
				public List<ICommon> getListOfItems(String source, IJSONForumDataParser parser) {
					List<ICommon> result = new LinkedList<ICommon>();
					
					try{
						JSONObject responseJson = new JSONObject(source);
						JSONArray commentsJson = responseJson.getJSONArray(Constants.SERVICE_API_TOPIC_COMMENTS);
						
						for(int i = 0 ; i < commentsJson.length(); ++i){
							JSONObject commentJson = commentsJson.getJSONObject(i);
							IFlatComment comment = parser.parseComment(commentJson);
							result.add(comment);
						}
						
					}catch(JSONException e){
						e.printStackTrace();
					}finally{
						return result;
					}
				
				}

				@Override
				public IContentJSONDataInterpreter setProperty(String name,
						String value) {
					_propMap.put(name, value);
					return this;
				}
			 
			};
		}
		return __interpreterInstanse;
	}
	
}
