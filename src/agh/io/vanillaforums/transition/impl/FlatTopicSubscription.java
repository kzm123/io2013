package agh.io.vanillaforums.transition.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopicSubscription;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.utils.Constants;
import agh.io.vanillaforums.transition.utils.IContentJSONDataInterpreter;

public class FlatTopicSubscription extends AbstractFlatContent implements IFlatTopicSubscription {

	private int _topicId;
	private int _userId;


	public FlatTopicSubscription(int id, Date creationDate,
			int topicId, int userId, IContentRemoteManager remoteManger) {
		super(ContentType.TOPIC_SUBSCRIPTION, id, creationDate, null, remoteManger.getAPIUrlBuilder().showTopicSubscriptionUrl(id), remoteManger);
		
		_topicId = topicId;
		_userId = userId;
	}

	@Override
	public IFlatTopic getTopic() 
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, 
			BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, 
			NotImplementedException, InternalServerErrorException {
		
		return _remoteContentManager.getTopic(_topicId);
	}

	@Override
	public IFlatUser getUser() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		
		return _remoteContentManager.getUser(_userId);
	}

	@Override
	public boolean delete() throws NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, RemoteObjectDoesNotExistException {
		return _remoteContentManager.deleteTopicSubscription(_id);
	}

	@Override
	public int getUserId() {
		return _userId;
	}
	
	@Override
	public int getTopicId() {
		return _topicId;
	}

	
	@Override
	protected void reloadContent(ICommon whatDownload) {
		// UNUSED
	}

	
	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
		
		sb	.append("FlatTopicSubscription ::")
			.append("\n\tid : ")
			.append(_id)
			.append("\n\tcreationDate : ")
			.append(Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString(_creationDate))
			.append("\n\ttopicId : ")
			.append(_topicId)
			.append("\n\tuserId : ")
			.append(_userId)
			.append("\n\n");
		
		return sb.toString();
	}
	
	private static IContentJSONDataInterpreter __interpreterInstanse = null;
	public static IContentJSONDataInterpreter getInterpreter(){
		if(__interpreterInstanse == null){
			__interpreterInstanse = new IContentJSONDataInterpreter() {
				
				private HashMap<String,String> _propMap = new HashMap<String,String>();
				
				@SuppressWarnings("finally")
				@Override
				public List<ICommon> getListOfItems(String source,  IJSONForumDataParser parser) {
					List<ICommon> result = new LinkedList<ICommon>();
					try {
						
						JSONObject jsonResponse = new JSONObject(source);
						JSONArray jsonSubscriptions = jsonResponse.getJSONArray(Constants.SERVICE_API_TOPIC_SUBSCRIPTIONS);
						
						for(int idx = 0 ; idx < jsonSubscriptions.length(); ++idx){
							JSONObject topicSubscriptionJson = jsonSubscriptions.getJSONObject(idx);
							FlatTopicSubscription topicSubscription = parser.parseTopicSubscription(topicSubscriptionJson);
							result.add(topicSubscription);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}finally{
						return result;
					}
				}
				
	
				@SuppressWarnings("finally")
				@Override
				public ICommon getItem(String source, IJSONForumDataParser parser) {
					ICommon result = null;
					try{
						JSONObject jsonResponse = new JSONObject(source);
						JSONObject jsonSubscription = jsonResponse.getJSONObject(Constants.SERVICE_CONTENT_TOPIC_SUBSCRIPTION);
						result = parser.parseTopicSubscription(jsonSubscription);
					}catch(JSONException e){
						e.printStackTrace();
					}finally{
						return result;
					}
				}


				@Override
				public IContentJSONDataInterpreter setProperty(String name,
						String value) {
					_propMap.put(name, value);
					return this;
				}
			};
		}
		return __interpreterInstanse;
	}

}
