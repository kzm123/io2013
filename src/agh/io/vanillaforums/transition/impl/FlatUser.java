package agh.io.vanillaforums.transition.impl;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.items.IFlatComment;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForumSubscription;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopicSubscription;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotFoundException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotSetedMandatoryFieldsException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.UserInfo;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.iteminfo.utils.UserRole;
import agh.io.vanillaforums.transition.utils.Constants;
import agh.io.vanillaforums.transition.utils.IContentJSONDataInterpreter;

public class FlatUser extends AbstractFlatContent implements IFlatUser {


	private String _name;
	private boolean _isActive;
	private Date _lastLoginAt;
	private String _email;
	private String _phone;
	private String _details;
	private UserRole _userRole;
	private boolean _isSuspended;
	private boolean _isModerator;
	private String _signature;

	public FlatUser( int id, Date creationDate,	Date updateDate, String apiUrl,
			String name, boolean isActive, Date lastLoginAt, String email, String phone, 
			String details, UserRole userRole, boolean isSuspended, boolean isModerator, String signature,
			IContentRemoteManager remoteManger) {
		super(ContentType.USER, id, creationDate, updateDate, apiUrl, remoteManger);
		
		_name = name;
		_isActive = isActive;
		_lastLoginAt = lastLoginAt;
		_email = email;
		_phone = phone;
		_details  = details;
		_userRole = userRole;
		_isSuspended = isSuspended;
		_isModerator = isModerator;
		_signature = signature;
	}

	@Override
	public UserInfo getInfo() {
		UserInfo result = new UserInfo();
		
		result.setUserName(_name)
				.setUserEmail(_email)
				.setUserPhone(_phone)
				.setUserDetails(_details)
				.setUserRole(_userRole)
				.setIsSuspended(_isSuspended)
				.setIsModerator(_isModerator)
				.setUserSignature(_signature)
				.setId(_id);
		
		return result;
	}
	
	@Override
	public String getName() {
		return _name;
	}

	@Override
	public boolean isActive() {
		return _isActive;
	}

	@Override
	public Date lastLoginAt() {
		return _lastLoginAt;
	}

	@Override
	public String getEmail() {
		return _email;
	}

	@Override
	public String getPhone() {
		return _phone;
	}

	@Override
	public String getDetails() {
		return _details;
	}

	@Override
	public UserRole getRole() {
		return _userRole;
	}

	@Override
	public boolean isModerator() {
		return _isModerator;
	}

	@Override
	public boolean isSuspended() {
		return _isSuspended;
	}

	@Override
	public String getSignature() {
		return _signature;
	}
	
	
	@Override
	public IFlatForumSubscription subscribeForum(IFlatForum targetForum) throws UnsupportedEncodingException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException {
		return _remoteContentManager.createForumSubscription(targetForum.getId(), _id);
	}

	@Override
	public IFlatTopicSubscription subscribeTopic(IFlatTopic targetTopic) throws UnsupportedEncodingException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, NotFoundException, InvalidRecordException {
		return _remoteContentManager.createTopicSubscription(targetTopic.getId(), _id);
	}

	@Override
	protected void reloadContent(ICommon whatDownload) {
		IFlatUser other = (IFlatUser) whatDownload;
		_name = other.getName();
		_isActive = other.isActive();
		_lastLoginAt = other.lastLoginAt();
		_email = other.getEmail();
		_phone = other.getPhone();
		_details = other.getDetails();
		_userRole = other.getRole();
		_isSuspended = other.isSuspended();
		_isModerator = other.isModerator();
	}

	@Override
	public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, 
		BadRequestException, UnprocessableEntityException, ServiceUnavaliableException,
		NotImplementedException, InternalServerErrorException {
		
		return _remoteContentManager.deleteUser(_id);
	}

	@Override
	public boolean update(UserInfo info) throws NotSetedMandatoryFieldsException, UnsupportedEncodingException, RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		 IFlatUser newContent = _remoteContentManager.updateUser(_id,info);
		 
		 if(newContent == null)
			 return false;
		 
		 reloadContent(newContent);
		 
		 return true;
	}

	@Override
	public FlatUserRelatedInfo getRelatedInfo() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		return _remoteContentManager.getUserRelatedInfo(_id);
	}

	@Override
	public boolean changePassword(String oldPassword, String newPassword) throws RemoteObjectDoesNotExistException, UnsupportedEncodingException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException {
		return _remoteContentManager.changeUserPassword(_id, oldPassword, newPassword);
	}

	@Override
	public List<IFlatTopic> getUserTopics() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		return _remoteContentManager.getUserTopics(_id);
	}

	@Override
	public List<IFlatComment> getUserComments() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException {
		return _remoteContentManager.getUserComments(_id);
	}

	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
		
		sb.append("FlatUser ::")
			.append("\n\tid : ")
			.append(_id)
			.append("\n\tcreated_at : ")
			.append(Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString(_creationDate))
			.append("\n\tupdated_at : ")
			.append(Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString(_updateDate))
			.append("\n\tname : ")
			.append(_name)
			.append("\n\tisActive : ")
			.append(_isActive)
			.append("\n\tlastLoginAt : ")
			.append(Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString(_lastLoginAt))
			.append("\n\temail : ")
			.append(_email)
			.append("\n\tphone : ")
			.append(_phone)
			.append("\n\tdetails : ")
			.append(_details)
			.append("\n\tuserRole : ")
			.append(_userRole.getValue())
			.append("\n\tisSuspended : ")
			.append(_isSuspended)
			.append("\n\tisModerator : ")
			.append(_isModerator)
			.append("\n\tsignature : ")
			.append(_signature)
			.append("\n\n");
		
		return sb.toString();
	}


	private static IContentJSONDataInterpreter __interpreterInstanse = null;
	public static IContentJSONDataInterpreter getInterpreter(){
		if(__interpreterInstanse == null){
			__interpreterInstanse = new IContentJSONDataInterpreter() {

				private HashMap<String,String> _propMap = new HashMap<String,String>();
				
				@Override
				public ICommon getItem(String source, IJSONForumDataParser parser) {
					ICommon result = null;
					try {
						JSONObject responseJson = new JSONObject(source);
						JSONObject userJson = responseJson.getJSONObject(Constants.SERVICE_CONTENT_USER);
						result = parser.parseUser(userJson);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					return result;
				}

				
				@SuppressWarnings("finally")
				@Override
				public List<ICommon> getListOfItems(String source,IJSONForumDataParser parser) {
					List<ICommon> result = new LinkedList<ICommon>();
					try {
						JSONObject responseJson = new JSONObject(source);
						JSONArray usersJson = responseJson.getJSONArray(Constants.SERVICE_API_USERS);
						
						for(int i = 0; i < usersJson.length(); ++i){
							JSONObject userJson = usersJson.getJSONObject(i);
							IFlatUser user = parser.parseUser(userJson);
							result.add(user);
						}
						
					} catch (JSONException e) {
						e.printStackTrace();
					}finally{
						return result;
					}
				}

				
				@Override
				public IContentJSONDataInterpreter setProperty(String name,	String value) {
					_propMap.put(name, value);
					return this;
				}
				
		
			};
		}
		return __interpreterInstanse;
	}
}
