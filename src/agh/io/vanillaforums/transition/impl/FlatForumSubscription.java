package agh.io.vanillaforums.transition.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForum;
import agh.io.vanillaforums.transition.interfaces.items.IFlatForumSubscription;
import agh.io.vanillaforums.transition.interfaces.items.IFlatUser;
import agh.io.vanillaforums.transition.interfaces.items.parsing.IJSONForumDataParser;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.utils.Constants;
import agh.io.vanillaforums.transition.utils.IContentJSONDataInterpreter;

public class FlatForumSubscription extends AbstractFlatContent implements IFlatForumSubscription {

	private int _forumId;
	private int _userId;

	public FlatForumSubscription(int id, Date creationDate,
								int forumId, int userId, IContentRemoteManager remoteManger) {
		super(ContentType.FORUM_SUBSCRIPTION, id, creationDate, null, remoteManger.getAPIUrlBuilder().showForumSubscriptionUrl(id), remoteManger);
		
		_forumId = forumId;
		_userId = userId;
	}


	@Override
	public IFlatForum getForum() 
			throws RemoteObjectDoesNotExistException, NotAuthorizedException, 
			BadRequestException, UnprocessableEntityException, 
			ServiceUnavaliableException, NotImplementedException, 
			InternalServerErrorException, InvalidRecordException {
		
		return _remoteContentManager.getForum(_forumId);
	}

	@Override
	public IFlatUser getUser() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		return _remoteContentManager.getUser(_userId);
	}

	@Override
	public boolean delete() throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException {
		return _remoteContentManager.deleteForumSubscription(_id);
	}
	
	@Override
	protected void reloadContent(ICommon whatDownload) {
		// UNUSED
	}

	@Override
	public int getForumId() {
		return _forumId;
	}

	@Override
	public int getUserId() {
		return _userId;
	}

	@Override
	public String toString(){
		StringBuffer sb = new StringBuffer();
		
		sb	.append("FlatForumSubscription ::")
			.append("\n\tid : ")
			.append(_id)
			.append("\n\tcreationDate : ")
			.append(Constants.SERVICE_UTIL_API_DATE_PARSER.dateToString(_creationDate))
			.append("\n\tforumId : ")
			.append(_forumId)
			.append("\n\tuserId : ")
			.append(_userId)
			.append("\n\n");
		
		return sb.toString();
	}
	
	
	private static IContentJSONDataInterpreter __interpreterInstanse = null;
	public static IContentJSONDataInterpreter getInterpreter(){
		if(__interpreterInstanse == null){
			__interpreterInstanse = new IContentJSONDataInterpreter() {

				private HashMap<String,String> _propMap = new HashMap<String,String>();
				
				@SuppressWarnings("finally")
				@Override
				public ICommon getItem(String source, IJSONForumDataParser parser) {
					ICommon result = null;
					try{
						JSONObject responseJson = new JSONObject(source);
						JSONObject forumSubscriptionJson = responseJson.getJSONObject(Constants.SERVICE_CONTENT_FORUM_SUBSCRIPTION);
						result = parser.parseForumSubscription(forumSubscriptionJson);
					}finally{
						return result;
					}
					
				}

				@SuppressWarnings("finally")
				@Override
				public List<ICommon> getListOfItems(String source, IJSONForumDataParser parser) {
					List<ICommon> result = new LinkedList<ICommon>();
					try{
						JSONObject responseJson = new JSONObject(source);
						JSONArray forumSubscripionsJson = responseJson.getJSONArray(Constants.SERVICE_API_FORUM_SUBSCRIPTIONS);
						
						for(int i = 0; i < forumSubscripionsJson.length(); ++i){
							JSONObject forumSubscriptionJson = forumSubscripionsJson.getJSONObject(i);
							FlatForumSubscription forumSubscription = parser.parseForumSubscription(forumSubscriptionJson);
							if(forumSubscription != null)
								result.add(forumSubscription);
						}
						
					}catch(JSONException e){
						e.printStackTrace();
					}finally{
						return result;
					}
				}

				@Override
				public IContentJSONDataInterpreter setProperty(String name,
						String value) {
					_propMap.put(name, value);
					return this;
				}
				
				
			};
		}
		return __interpreterInstanse;
	}
}
