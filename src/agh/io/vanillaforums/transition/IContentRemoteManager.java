package agh.io.vanillaforums.transition;

import agh.io.vanillaforums.transition.interfaces.items.ICommon;
import agh.io.vanillaforums.transition.interfaces.managing.ICategoryRemoteManager;
import agh.io.vanillaforums.transition.interfaces.managing.ICommentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.managing.IForumRemoteManager;
import agh.io.vanillaforums.transition.interfaces.managing.IForumSubscriptionRemoteManager;
import agh.io.vanillaforums.transition.interfaces.managing.ITopicRemoteManager;
import agh.io.vanillaforums.transition.interfaces.managing.ITopicSubscriptionRemoteaManager;
import agh.io.vanillaforums.transition.interfaces.managing.IUserRemoteManager;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.BadRequestException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InternalServerErrorException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.InvalidRecordException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotImplementedException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.RemoteObjectDoesNotExistException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.ServiceUnavaliableException;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.UnprocessableEntityException;
import agh.io.vanillaforums.transition.iteminfo.utils.ContentType;
import agh.io.vanillaforums.transition.utils.IAPIUrlBuilder;

public interface IContentRemoteManager extends ICategoryRemoteManager,
		IForumRemoteManager, ITopicRemoteManager, ICommentRemoteManager,
		IUserRemoteManager, ITopicSubscriptionRemoteaManager, IForumSubscriptionRemoteManager {

	public ICommon reloadWhithAPIUrl(String url, ContentType typeOfContent) throws RemoteObjectDoesNotExistException, NotAuthorizedException, BadRequestException, UnprocessableEntityException, ServiceUnavaliableException, NotImplementedException, InternalServerErrorException, InvalidRecordException;
	
	public boolean logIn(String user, String password);
	public void logOut();
	public IAPIUrlBuilder getAPIUrlBuilder();
}
