package agh.io.vanillaforums;

import java.util.List;

import agh.io.vanillaforums.transition.ContentRemoteManager;
import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.IFlatCategory;
import agh.io.vanillaforums.transition.interfaces.managing.exceptions.NotAuthorizedException;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends Activity {

	private IContentRemoteManager remoteManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		remoteManager = ContentRemoteManager.getDefaultInstance();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_login:
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			break;
		case R.id.action_authors:
			showAlertDialog(R.string.authors_title, R.string.authors_desc);
			break;
		case R.id.action_settings:
			Intent settingsIntent = new Intent(this, SettingsActivity.class);
			startActivity(settingsIntent);
			break;
		case R.id.action_last_topics:
			Intent lastTopicsIntent = new Intent(this, LastTopicsActivity.class);
			startActivity(lastTopicsIntent);
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();
		new RefreshCategoriesTask().execute();
	}
	
	private void showAlertDialog(int titleId, int messageId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(messageId).setTitle(titleId);
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	private void showErrorDialog(int messageId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(messageId).setTitle(R.string.error);
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				MainActivity.this.finish();
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	public class RefreshCategoriesTask extends AsyncTask<Object, Object, Object> {
		
		private ProgressDialog dialog = new ProgressDialog(MainActivity.this);
		
		@Override
		protected void onPreExecute() {
			dialog.setMessage("Please wait...");
			dialog.show();
		}
		
		@Override
		protected void onPostExecute(Object result) {
			if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
		}

		@Override
		protected Object doInBackground(Object... params) {
			refresh();
			return null;
		}
		
		private void refresh() {
			try {
				final List<IFlatCategory> categories = remoteManager.getAllCategories();
		
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						final ListView listView = (ListView) findViewById(R.id.list_categories);
						CategoryListAdapter adapter = new CategoryListAdapter(MainActivity.this, categories);
						listView.setAdapter(adapter);
						listView.setOnItemClickListener(new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
								Intent intent = new Intent(MainActivity.this, CategoryActivity.class);
								IFlatCategory category = (IFlatCategory) listView.getItemAtPosition(position);
								intent.putExtra("id", category.getId());
								intent.putExtra("name", category.getName());
								startActivity(intent);
							}
						});
					}
				});
				
			} catch (NotAuthorizedException e) {
				Intent intent = new Intent(MainActivity.this, LoginActivity.class);
				startActivity(intent);
			} catch (Exception e) {
				if (dialog.isShowing()) {
		            dialog.dismiss();
		        }
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						showErrorDialog(R.string.error_connection);
					}
				});
				
			}
		}
		
	}
	

}
