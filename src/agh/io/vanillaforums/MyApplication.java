package agh.io.vanillaforums;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import android.app.Application;

public class MyApplication extends Application {

    public static final List<Integer> myTopics = new LinkedList<Integer>();
    public static final Map<Integer, IFlatTopic> localTopics = new HashMap<Integer, IFlatTopic>();
    
}