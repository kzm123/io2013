/**
 *   Author: Aral
 *   Date: 10 cze 2013 22:47:50
 */
package agh.io.vanillaforums.logic;

import java.util.List;

import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;

public class ContentStateComparer
{
    public static void compareStates( List<IFlatTopic> remoteState, List<IFlatTopic> localState )
    {
        for( IFlatTopic lastEddited: localState )
        {
            for( IFlatTopic newContent: remoteState )
            {
                if( lastEddited.getId() == newContent.getId() )
                {
                    if( lastEddited.getCommentsCount() != newContent.getCommentsCount() )
                        newContent.setHasChanged( true );
                }
            }
        }
    }
}
