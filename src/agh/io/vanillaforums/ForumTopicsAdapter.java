package agh.io.vanillaforums;

import java.util.List;

import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ForumTopicsAdapter extends BaseAdapter {
 
    private List<IFlatTopic> listData;
 
    private LayoutInflater layoutInflater;
 
    public ForumTopicsAdapter(Context context, List<IFlatTopic> listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }
 
    @Override
    public int getCount() {
        return listData.size();
    }
 
    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.list_topics, null);
            holder = new ViewHolder();
            holder.titleView = (TextView) convertView.findViewById(R.id.list_topic_title);
            holder.dateView = (TextView) convertView.findViewById(R.id.list_topic_date);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        
        holder.titleView.setText(listData.get(position).getTitle());
        holder.dateView.setText(listData.get(position).getUpdateDate().toString());
 
        return convertView;
    }
 
    static class ViewHolder {
        TextView titleView;
        TextView dateView;
    }
 
}