package agh.io.vanillaforums;

import java.util.List;

import agh.io.vanillaforums.transition.ContentRemoteManager;
import agh.io.vanillaforums.transition.IContentRemoteManager;
import agh.io.vanillaforums.transition.interfaces.items.IFlatTopic;
import agh.io.vanillaforums.transition.iteminfo.TopicInfo;
import agh.io.vanillaforums.transition.utils.Constants;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;

public class ForumActivity extends Activity {

	private IContentRemoteManager remoteManager;
	private int id;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_forum);
		
		remoteManager = ContentRemoteManager.getDefaultInstance();

		Bundle extras = getIntent().getExtras();
		setTitle(extras.getString("name"));
		id = extras.getInt("id");
		
		new RefreshTopicsTask().execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.forum, menu);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_login:
			Intent intent = new Intent(this, LoginActivity.class);
			startActivity(intent);
			break;
		case R.id.action_authors:
			showAlertDialog(R.string.authors_title, R.string.authors_desc);
			break;
		case R.id.action_new_topic:
			AlertDialog.Builder alert = new AlertDialog.Builder(this);

			alert.setTitle("New topic");
			alert.setMessage("Enter topic name");

			final EditText input = new EditText(this);
			alert.setView(input);

			alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				String value = input.getText().toString();
				new NewTopicTask(value).execute();
			  }
			});

			alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			  public void onClick(DialogInterface dialog, int whichButton) {
			    // Canceled.
			  }
			});

			alert.show();
			break;
		case R.id.action_settings:
			Intent settingsIntent = new Intent(this, SettingsActivity.class);
			startActivity(settingsIntent);
			break;
		case R.id.action_last_topics:
			Intent lastTopicsIntent = new Intent(this, LastTopicsActivity.class);
			startActivity(lastTopicsIntent);
			break;
		}
		
		return super.onOptionsItemSelected(item);
	}
	
	private void showAlertDialog(int titleId, int messageId) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(messageId).setTitle(titleId);
		builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	public class RefreshTopicsTask extends AsyncTask<Object, Object, Object> {
		
		private ProgressDialog dialog = new ProgressDialog(ForumActivity.this);
		
		@Override
		protected void onPreExecute() {
			dialog.setMessage("Please wait...");
			dialog.show();
		}
		
		@Override
		protected void onPostExecute(Object result) {
			if (dialog.isShowing()) {
	            dialog.dismiss();
	        }
		}

		@Override
		protected Object doInBackground(Object... params) {
			refresh();
			return null;
		}
		
		private void refresh() {
			try {
				remoteManager.logIn(Constants.API_USERNAME, Constants.API_PASSWORD);
				
				final List<IFlatTopic> topics = remoteManager.getForumTopics(id);
				
				runOnUiThread(new Runnable() {
					
					@Override
					public void run() {
						final ListView listView = (ListView) findViewById(R.id.list_topics);
						ForumTopicsAdapter adapter = new ForumTopicsAdapter(ForumActivity.this, topics);
						listView.setAdapter(adapter);
						listView.setOnItemClickListener(new OnItemClickListener() {
							@Override
							public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
								Intent intent = new Intent(ForumActivity.this, TopicActivity.class);
								IFlatTopic topic = (IFlatTopic) listView.getItemAtPosition(position);
								intent.putExtra("id", topic.getId());
								intent.putExtra("name", topic.getTitle());
								startActivity(intent);
							}
						});
					}
				});
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
	}
	
	public class NewTopicTask extends AsyncTask<Object, Object, Object> {
		
		private String title;
		
		public NewTopicTask(String title) {
			this.title = title;
		}
		
		@Override
		public void onPostExecute(Object result) {
			new RefreshTopicsTask().execute();
		}

		@Override
		protected Object doInBackground(Object... params) {
			try {
				TopicInfo topic = new TopicInfo();
				topic.setTopicTitle(title);
				topic.setParentForumId(id);
				topic.setTopicBody(title);
				remoteManager.createTopic(topic);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
			return null;
		}
	}
	

}
